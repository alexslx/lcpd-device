﻿using System.Collections.Generic;
using System.Linq;

namespace LCPDDevice.API.Collections
{
	/// <summary>
	/// Collection root for common GTA objects.
	/// This also act as a <see cref="GTACollectionCategory"/> for orphan items.
	/// </summary>
	/// <seealso cref="GTACollectionCategory"/>
	/// <seealso cref="GTACollectionItem"/>
	public class GTACollection : GTACollectionCategory
	{
		private Dictionary<string, GTACollectionCategory> catList;

		/// <summary>
		/// Object constructor.
		/// </summary>
		public GTACollection()
		{
			catList = new Dictionary<string, GTACollectionCategory>();
		}

		/// <summary>
		/// Indicate if the category currently exists.
		/// </summary>
		/// <param name="Id">String containing the key name.</param>
		/// <returns>True if the key exists, false otherwise.</returns>
		public bool CategoryExists(string Id)
		{
			if( this.catList.ContainsKey(Id) )
				return true;

			return false;
		}

		/// <summary>
		/// Get all <see cref="GTACollectionCategory"/> as an array.
		/// </summary>
		/// <returns>Array of <see cref="GTACollectionCategory"/> objects.</returns>
		public GTACollectionCategory[] GetCategoryList()
		{
			return this.catList.Values.ToArray();
		}

		/// <summary>
		/// Add an new category to the collection list.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.</param>
		/// <param name="Value"><see cref="GTACollectionCategory"/> to be added.</param>
		/// <param name="Update">Flag indicating if the parameter should be updated if already found.</param>
		/// <returns>True if the addition or update is successful, false otherwise.</returns>
		public bool AddCategory(string Id, GTACollectionCategory Value, bool Update = true)
		{
			if( CategoryExists(Id) && !Update )
				return false;

			this.catList[Id] = Value;
			return true;
		}

		/// <summary>
		/// Get a category by its unique id.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.s</param>
		/// <returns><see cref="GTACollectionCategory"/> object if found, null otherwise.</returns>
		public GTACollectionCategory GetCategoryById(string Id)
		{
			if( !CategoryExists(Id) )
				return null;

			return this.catList[Id];
		}

		/// <summary>
		/// Get a item by its unique id. This method search on all categories.
		/// </summary>
		/// <param name="id">String containing the parameter unique name.</param>
		/// <returns><see cref="GTACollectionItem"/> if found, null otherwise.</returns>
		new public GTACollectionItem GetItemById(string id)
		{
			GTACollectionItem item = base.GetItemById(id);
			if( item != null )
				return item;

			GTACollectionCategory[] catList = this.GetCategoryList();
			foreach(GTACollectionCategory cat in catList)
			{
				item = cat.GetItemById(id);
				if( item != null )
					return item;
			}
			
			return null;
		}
	}
}
