﻿using System.Collections.Generic;
using System.Linq;

namespace LCPDDevice.API.Collections
{
	/// <summary>
	/// GTA Collection category class.
	/// </summary>
	public class GTACollectionCategory
	{
		private string id;
		private string name;

		private Dictionary<string , GTACollectionItem> itemList;

		/// <summary>
		/// Object Constructor.
		/// </summary>
		public GTACollectionCategory()
		{
			itemList = new Dictionary<string, GTACollectionItem>();
		}

		/// <summary>
		/// Set object unique id.
		/// </summary>
		/// <param name="Id">String representing an unique id.</param>
		public void SetId(string Id)
		{
			this.id = Id;
		}

		/// <summary>
		/// Get current object unique id.
		/// </summary>
		/// <returns>String representing object unique id.</returns>
		public string GetId()
		{
			return this.id;
		}

		/// <summary>
		/// Set object name.
		/// </summary>
		/// <param name="Name">String representing the object name.</param>
		public void SetCategoryName(string Name)
		{
			this.name = Name;
		}

		/// <summary>
		/// Get current object name.
		/// </summary>
		/// <returns>String representing object name.</returns>
		public string GetCategoryName()
		{
			return this.name;
		}

		/// <summary>
		/// Indicate if the cat currently exists.
		/// </summary>
		/// <param name="Id">String containing the key name.</param>
		/// <returns>True if the key exists, false otherwise.</returns>
		public bool ItemExists(string Id)
		{
			if( this.itemList.ContainsKey(Id) )
				return true;

			return false;
		}

		/// <summary>
		/// Get number of items on this category
		/// </summary>
		/// <returns>Integer containing the number of items on this category.</returns>
		public int GetSize()
		{
			return this.itemList.Count;
		}

		/// <summary>
		/// Get all <see cref="GTACollectionItem"/> as an array.
		/// </summary>
		/// <returns>Array of <see cref="GTACollectionItem"/> objects.</returns>
		public GTACollectionItem[] GetItemList()
		{
			return this.itemList.Values.ToArray();
		}

		/// <summary>
		/// Add an new cat to the collection list.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.</param>
		/// <param name="Value"><see cref="GTACollectionItem"/> object to be added.</param>
		/// <param name="Update">Flag indicating if the parameter should be updated if already found.</param>
		/// <returns>True if the addition or update is successful, false otherwise.</returns>
		public bool AddItem(string Id, GTACollectionItem Value, bool Update = true)
		{
			if( ItemExists(Id) && !Update )
				return false;

			this.itemList[Id] = Value;
			return true;
		}

		/// <summary>
		/// Get an item by its unique id.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.</param>
		/// <returns><see cref="GTACollectionItem"/> if found, null otherwise.</returns>
		public GTACollectionItem GetItemById(string Id)
		{
			if( !ItemExists(Id) )
				return null;

			return this.itemList[Id];
		}
	}
}
