﻿using System.Collections.Specialized;

namespace LCPDDevice.API.Collections
{
	/// <summary>
	/// Represents a single GTA Collection item.
	/// </summary>
	public class GTACollectionItem
	{
		private string id;

		private string value;

		private NameValueCollection itemList;

		/// <summary>
		/// Object constructor.
		/// </summary>
		public GTACollectionItem()
		{
			itemList	= new NameValueCollection();
			value		= "";
		}

		/// <summary>
		/// Set object unique id.
		/// </summary>
		/// <param name="Id">String representing an unique id.</param>
		public void SetId(string Id)
		{
			this.id = Id;
		}

		/// <summary>
		/// Get current object unique id.
		/// </summary>
		/// <returns>String representing object unique id.</returns>
		public string GetId()
		{
			return this.id;
		}

		/// <summary>
		/// Set object value.
		/// </summary>
		/// <param name="value">String representing a value.</param>
		public void SetValue(string value)
		{
			this.value = value;
		}

		/// <summary>
		/// Get current object value.
		/// </summary>
		/// <param name="Default">String containing the default value to be returned if the parameter is empty.</param>
		/// <returns>String representing object value.</returns>
		public string GetValue(string Default = "")
		{
			if( string.IsNullOrWhiteSpace(this.value) )
				return Default;

			return this.value;
		}

		/// <summary>
		/// Indicate if the parameter currently exists.
		/// </summary>
		/// <param name="Id">String containing the key name.</param>
		/// <returns>True if the key exists, false otherwise.</returns>
		public bool ParamExists(string Id)
		{
			if( this.itemList[Id] != null )
				return true;

			return false;
		}

		/// <summary>
		/// Get all parameters as <see cref="NameValueCollection"/> object.
		/// </summary>
		/// <returns><see cref="NameValueCollection"/> with all parameters.</returns>
		public NameValueCollection GetAllParams()
		{
			return this.itemList;
		}

		/// <summary>
		/// Add an new parameter to the collection list.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.</param>
		/// <param name="Value">String containing the parameter value.</param>
		/// <param name="Update">Flag indicating if the parameter should be updated if already found.</param>
		/// <returns>True if the addition or update is successful, false otherwise.</returns>
		public bool AddParam(string Id, string Value, bool Update = true)
		{
			if( ParamExists(Id) && !Update )
				return false;

			this.itemList[Id] = Value;
			return true;
		}

		/// <summary>
		/// Get a parameter by its unique id.
		/// </summary>
		/// <param name="Id">String containing the parameter unique name.s</param>
		/// <param name="Default">String containing the default value to be returned if the parameter is not found.</param>
		/// <returns>String with the parameter value, or Default value if not found.</returns>
		public string GetParamById(string Id, string Default="")
		{
			if( !ParamExists(Id) )
				return Default;

			return this.itemList[Id];
		}
	}
}
