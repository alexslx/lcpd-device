﻿using GTA;
using LCPDDevice.API.App;
using LCPDDevice.API.Managers;
using LCPDDevice.Configs;

namespace LCPDDevice.API
{
	/// <summary>
	/// Object containing all informations about an application. Used by the <see cref="AppManager"/>.
	/// </summary>
	/// <seealso cref="AppUI"/>
	/// <seealso cref="AppManager"/>
	public class AppInfo
	{

		/// <summary>
		/// Application Unique ID
		/// </summary>
		public string AppID;

		/// <summary>
		/// Application Name
		/// </summary>
		public string AppName;

		/// <summary>
		/// Application Directory
		/// </summary>
		public string AppDir;

		/// <summary>
		/// Application Icon resource (filename).
		/// </summary>
		public Texture AppIcon;

		/// <summary>
		/// Application DLL resource (filename).
		/// </summary>
		public string AppDLL;

		/// <summary>
		/// Define if the application is enabled or not.
		/// </summary>
		public bool Enabled;

		/// <summary>
		/// Define if the application is an internal version (embedded).
		/// </summary>
		public bool Internal;

		/// <summary>
		/// Current <see cref="AppUI"/> instance.
		/// </summary>
		public AppUI UIClass;

		/// <summary>
		/// Current <see cref="AppScript"/> instance.
		/// </summary>
		public AppScript ScriptClass;

		/// <summary>
		/// Application <see cref="AppConfig">Settings</see>.
		/// </summary>
		public AppConfig Settings;
	}
}
