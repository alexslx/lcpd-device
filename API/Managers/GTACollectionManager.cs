﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.XPath;
using LCPDDevice.API.Collections;
using LCPDDevice.Helpers;
using LCPDDevice.Loaders;

namespace LCPDDevice.API.Managers
{
	/// <summary>
	/// GTA Collection manager.
	/// </summary>
	public class GTACollectionManager
	{
		private const string COLLECTIONS_PATH = LCPDDevice.DATA_PATH + "collections/";

		private List<GTACollection> collectionList;

		private static GTACollectionManager _instance;

		private GTACollectionManager()
		{
			collectionList = new List<GTACollection>();
		}

		internal void Load()
		{
			collectionList.Clear();

			string[] fileList = Directory.GetFiles(COLLECTIONS_PATH, "*.xml", SearchOption.TopDirectoryOnly);
			foreach( string filename in fileList )
			{
				GTACollection col = CollectionLoader.LoadCollectionFromFile(filename);
				if( col != null )
					collectionList.Add(col);
			}
		}

		/// <summary>
		/// Return a valid instance of this class or create a new one if necessary.
		/// </summary>
		/// <returns><see cref="GTACollectionManager"/> valid instance.</returns>
		public static GTACollectionManager GetInstance()
		{
			if( _instance == null )
				_instance = new GTACollectionManager();

			return _instance;
		}

		/// <summary>
		/// Return a collection found by its Id.
		/// </summary>
		/// <param name="Id">String containing the collection Id to be found.</param>
		/// <returns>GTACollection found or null otherwise.</returns>
		public GTACollection GetCollection(string Id)
		{
			foreach(GTACollection item in this.collectionList)
			{
				if( item.GetId().Equals(Id, StringComparison.OrdinalIgnoreCase) )
					return item;
			}

			return null;
		}
	}
}
