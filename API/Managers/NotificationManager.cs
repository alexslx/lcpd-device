﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using GTA;
using LCPDDevice.API.App;
using LCPDDevice.Main;
using LCPDDevice.Helpers;

namespace LCPDDevice.API.Managers
{
	/// <summary>
	/// Manages all <see cref="NotificationInfo">notifications</see>.
	/// </summary>
	public class NotificationManager
	{
		private Dictionary<string, NotificationInfo> notifList = new Dictionary<string, NotificationInfo>();

		internal static GTA.Font notifTitleFont;
		internal static GTA.Font notifTextFont;

		internal const int NOTIFICATION_WIDTH  = 385;
		internal const int NOTIFICATION_HEIGHT = 80;
		internal const int NOTIFICATION_STARTY = 10;

		internal const int NOTIFICATION_ICON_WIDTH  = 64;
		internal const int NOTIFICATION_ICON_HEIGHT = 64;

		// Singleton
		private static NotificationManager _instance;

		/// <summary>
		/// Get the current NotificationManager valid instance.
		/// </summary>
		/// <returns>NotificationManager instance</returns>
		public static NotificationManager GetInstance()
		{
			if( _instance == null )
				_instance = new NotificationManager();

			return _instance;
		}

		private NotificationManager()
		{
			notifTitleFont	= new GTA.Font(20f, GTA.FontScaling.Pixel);
			notifTextFont	= new GTA.Font(14f, GTA.FontScaling.Pixel);
		}

		/// <summary>
		/// Destroy the NotificationManager class.
		/// </summary>
		~NotificationManager()
		{
			if( notifTitleFont != null )
				notifTitleFont.Dispose();

			if( notifTextFont != null )
				notifTextFont.Dispose();

			notifTitleFont	= null;
			notifTextFont	= null;
		}

		/// <summary>
		/// Add a <see cref="NotificationInfo">notification</see> to the current notification queue.
		/// </summary>
		/// <param name="app">Owner <see cref="AppBase">application</see> of this notification.</param>
		/// <param name="notif"><see cref="NotificationInfo">notification</see> structure.</param>
		/// <param name="notifID">String containing an <see cref="StringHelper.IsValidID">unique id</see> for this notification (optional).</param>
		/// <returns>String containing the unique id of newly created notification, null otherwise.</returns>
		public string AddNotification(AppBase app, NotificationInfo notif, string notifID = "")
		{
			// Empty message
			if( string.IsNullOrWhiteSpace(notif.Message) )
				return null;

			// Without unique id
			if( string.IsNullOrWhiteSpace(notifID) )
				notifID = this.GetNowTickets().ToString();

			// Invalid unique id
			if( !StringHelper.IsValidID(notifID) )
				return null;

			AppInfo appInfo = app.GetAppInfo();

			// Concatenate unique id with app id to make it app-related.
			notifID			= appInfo.AppID + '#' + notifID;
			notif.AppName	= appInfo.AppName;
			notif.AppIcon	= appInfo.AppIcon;

			return AddNotification(notif, notifID);
		}

		/// <summary>
		/// Add a <see cref="NotificationInfo">notification</see> to the current notification queue.
		/// </summary>
		/// <param name="notif"><see cref="NotificationInfo">notification</see> structure.</param>
		/// <param name="notifID">String containing an <see cref="StringHelper.IsValidID">unique id</see> for this notification (optional).</param>
		/// <returns>String containing the unique id of newly created notification, null otherwise.</returns>
		internal string AddNotification(NotificationInfo notif, string notifID = "")
		{
			// Empty application name
			if( string.IsNullOrWhiteSpace(notif.AppName) )
				notif.AppName = LCPDDevice.APP_NAME;

			// Application without icon
			if( notif.AppIcon == null )
				notif.AppIcon = DeviceUI.GetDefaultIcon();

			// No unique id
			if( string.IsNullOrWhiteSpace(notifID) )
				notifID = this.GetNowTickets().ToString();

			// Convert unique id to UPPERCASE
			notifID			= notifID.ToUpperInvariant();

			// Default values
			notif.Duration	= Math.Max(3000, notif.Duration);
			notif.endTime	= -1L;
			notif.Id		= notifID;

			// Add or update the notification
			if( this.notifList.ContainsKey(notifID) )
				this.notifList[notifID] = notif;
			else
				this.notifList.Add(notifID, notif);

			return notifID;
		}

		/// <summary>
		/// Remove a notification from the system
		/// </summary>
		/// <param name="app">Application that is owner of this notification.</param>
		/// <param name="notifID">String containing the notification id to be removed.</param>
		/// <returns>True if the notification is found and removed, false otherwise.</returns>
		public bool RemoveNotificationById(AppBase app, string notifID)
		{
			if( string.IsNullOrWhiteSpace(notifID) )
				return false;

			if( !notifID.Contains('#') && app != null )
				notifID = app.GetAppInfo().AppID + '#' + notifID;

			notifID = notifID.ToUpperInvariant();

			if( this.notifList.ContainsKey(notifID) )
				return this.notifList.Remove(notifID);

			return false;
		}

		/// <summary>
		/// Return a list containing all current notifications on the queue.
		/// </summary>
		/// <returns>List of <see cref="NotificationInfo"/>.</returns>
		internal List<NotificationInfo> GetNotificationList()
		{
			return this.notifList.Values.ToList();
		}

		/// <summary>
		/// Register on the drawer.
		/// </summary>
		/// <param name="basescript">Script class</param>
		internal void RegisterDrawer(LCPDDevice basescript)
		{
			basescript.PerFrameDrawing += new GraphicsEventHandler(this.DrawNotification);
		}

		/// <summary>
		/// Get the current system tickets in milliseconds.
		/// </summary>
		/// <returns>Long Value containing the system tickets from epoch.</returns>
		internal long GetNowTickets()
		{
			long nowTickets = -1L;
			try
			{
				nowTickets = Convert.ToInt64((DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
			}
			catch(Exception){}

			return nowTickets;
		}

		/// <summary>
		/// Get current notification alpha channel based on the remaining duration.
		/// </summary>
		/// <param name="notifInfo"><see cref="NotificationInfo"/> structure.</param>
		/// <returns>Integer Value containing an alpha channel (0-255).</returns>
		internal int GetNotificationAlpha(NotificationInfo notifInfo)
		{
			float alpha		= 255;
			long remaining	= notifInfo.endTime - GetNowTickets();

			if( remaining < (notifInfo.Duration * 0.3) )
				alpha = Math.Max(0, (255f / notifInfo.Duration) * (float)(remaining / 2));

			return (int)alpha;
		}

		/// <summary>
		/// Draw one notification in the given rectangle
		/// </summary>
		/// <param name="notifInfo"><see cref="NotificationInfo">Notification</see> to be displayed.</param>
		/// <param name="e">Graphical object</param>
		/// <param name="X">X coordinate where the notification will be drawn.</param>
		/// <param name="Y">Y coordinate where the notification will be drawn.</param>
		/// <param name="W">Notification rectangle width.</param>
		/// <param name="H">Notification rectangle height.</param>
		/// <param name="applyAlpha">Should the alpha channel be applied on the notification.</param>
		internal void DrawNotification(NotificationInfo notifInfo, GraphicsEventArgs e, float X, float Y, float W, float H, bool applyAlpha = true)
		{
			//TODO: Find a way to apply alpha to application icon.
			int alpha		= 255; //applyAlpha ? GetNotificationAlpha(notifInfo) : 255;
			string title	= string.IsNullOrWhiteSpace(notifInfo.AppName) ? LCPDDevice.APP_NAME : notifInfo.AppName;

			RectangleF notifRect	= new RectangleF(X, Y, W, H);
			RectangleF iconRect		= new RectangleF(X, Y, NOTIFICATION_ICON_WIDTH, NOTIFICATION_ICON_HEIGHT);

			// Draw the rectangle
			e.Graphics.DrawRectangle(notifRect, Color.FromArgb(alpha, Color.Black));

			// Draw the icon
			iconRect.Offset(10, 8);
			e.Graphics.DrawSprite(notifInfo.AppIcon, iconRect, Color.FromArgb(alpha, Color.Transparent));

			// Draw the title
			notifRect.Offset( (NOTIFICATION_ICON_WIDTH + 20), 4f);
			notifRect.Size -= new Size( (NOTIFICATION_ICON_WIDTH + 20), 4);
			e.Graphics.DrawText(title, notifRect, TextAlignment.SingleLine, Color.FromArgb(alpha, Color.White), notifTitleFont);

			// Draw the text
			notifRect.Offset(0, 24f);
			notifRect.Height -= 24f;
			e.Graphics.DrawText(notifInfo.Message, notifRect, TextAlignment.WordBreak, Color.FromArgb(alpha, Color.White), notifTextFont);
		}

		/// <summary>
		/// Draw one notification from the queue to the screen.
		/// </summary>
		/// <param name="sender">Sender object</param>
		/// <param name="e">GraphicsEventArgs</param>
		private void DrawNotification(object sender, GraphicsEventArgs e)
		{
			if( this.notifList.Count == 0 )
				return;

			NotificationInfo notifInfo = this.notifList.Values.First();
			long nowTickets = GetNowTickets();
			long remaining  = notifInfo.endTime - nowTickets;

			if(notifInfo.endTime == -1)
			{
				notifInfo.endTime				= nowTickets + notifInfo.Duration;
				remaining						= notifInfo.Duration;
				//this.notifList[notifInfo.Id]	= notifInfo;
			}

			if( (int)remaining < 1 )
			{
				this.notifList.Remove(notifInfo.Id);
				return;
			}

			int X = (int)(Game.Resolution.Width / 2 - NOTIFICATION_WIDTH / 2);
			DrawNotification(notifInfo, e, X, NOTIFICATION_STARTY, NOTIFICATION_WIDTH, NOTIFICATION_HEIGHT, true);
		}
	}
}
