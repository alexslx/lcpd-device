﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LCPDDevice.API.App;
using LCPDDevice.Applications;
using LCPDDevice.Configs;
using LCPDDevice.Helpers;
using LCPDDevice.Loaders;
using LCPDDevice.Main;

namespace LCPDDevice.API.Managers
{
	/// <summary>
	/// Application manager. Responsible to controll all app instances.
	/// </summary>
	/// <seealso cref="AppUI"/>
	/// <seealso cref="AppScript"/>
	/// <seealso cref="AppInfo"/>
	public class AppManager
	{
		private Dictionary<string, AppInfo> appList = new Dictionary<string, AppInfo>();

		private static AppManager _instance;

		/// <summary>
		/// Return a valid instance of this class or create a new one if necessary.
		/// </summary>
		/// <returns><see cref="AppManager"/> valid instance.</returns>
		public static AppManager GetInstance()
		{
			if(_instance == null)
				_instance = new AppManager();

			return _instance;
		}

		private AppManager()
		{
			
		}

		/// <summary>
		/// Load all <see cref="AppInfo">applications</see> information.
		/// </summary>
		public void Load()
		{
			this.LoadAppList();
		}

		/// <summary>
		/// Add an <see cref="AppInfo"/> to the current loaded list.
		/// </summary>
		/// <param name="id">String containing the application ID.</param>
		/// <param name="appInfo"><see cref="AppInfo"/> structure.</param>
		public void AddApplication(string id, AppInfo appInfo)
		{
			id = id.Trim();

			DebugHelper.Instance.DebugMessage("Adding App: " + id);

			appList.Add(id, appInfo);
		}

		/// <summary>
		/// Check if any application exists with the given ID.
		/// </summary>
		/// <param name="id">String containing the application ID.</param>
		/// <returns>True if the app exists, false otherwise.</returns>
		public bool AppExists(string id)
		{
			id = id.Trim();

			if( appList.ContainsKey(id) )
				return true;

			return false;
		}

		/// <summary>
		/// Get an <see cref="AppInfo"/> by its Id.
		/// </summary>
		/// <param name="id">String containing the application ID</param>
		/// <returns><see cref="AppInfo"/> if found. Blank structure otherwise.</returns>
		public AppInfo GetAppById(string id)
		{
			id = id.Trim();

			if( !this.AppExists(id) )
				return null;

			return appList[id];
		}

		/// <summary>
		/// Get a list of all loaded applications.
		/// </summary>
		/// <returns>A list containing all loaded <see cref="AppInfo">applications</see>.</returns>
		public List<AppInfo> GetAppList()
		{
			return this.appList.Values.ToList();
		}

		/// <summary>
		/// Get a list of all loaded applications that contains scripts.
		/// </summary>
		/// <returns>A list containing all loaded <see cref="AppInfo">applications</see>.</returns>
		public List<AppInfo> GetAppScriptList()
		{
			return this.appList.Values.Where(x => x.ScriptClass != null).ToList();
		}

		/// <summary>
		/// Launch an Application UI.
		/// </summary>
		/// <param name="id">Application ID.</param>
		public void LaunchApp(string id)
		{
			try
			{
				AppInfo app = AppManager.GetInstance().GetAppById(id);
				if( app != null && app.UIClass != null )
				{
					Device.GetInstance().SetCurrentApp(app.UIClass);
					Device.GetInstance().Open();
				}
				else
				{
					LoggerHelper.DebugMessage("Invalid application to launch: " + id);

					NotificationInfo notif = new NotificationInfo("Invalid Application.\n" + id, 2500);
				}
			}
			catch( Exception e )
			{
				DebugHelper.Instance.DebugMessage("Error while loading App: " + e.ToString());
				GoToHome();
			}
		}

		/// <summary>
		/// Close current application and open the homescreen.
		/// </summary>
		public void GoToHome()
		{
			Device.GetInstance().SetCurrentApp( new Homescreen() );
			Device.GetInstance().Open();
		}

		/// <summary>
		/// Search into app directory and load all enabled and valid applications.
		/// </summary>
		private void LoadAppList()
		{
			List<string> dirs = new List<string>(Directory.EnumerateDirectories(LCPDDevice.APPS_PATH));

			this.appList.Clear();
			foreach( var dir in dirs )
			{
				string DirName	= dir.Substring(dir.LastIndexOf("/") + 1);
				AppInfo appInfo	= new AppInfo();

				if( this.LoadAppSettings(DirName, ref appInfo) == true && appInfo.Enabled == true )
				{
					if( !AssemblyLoader.LoadAppAssembly(ref appInfo) )
						continue;

					if( appInfo.UIClass != null )
					{
						appInfo.UIClass.SetAppInfo(appInfo);
						appInfo.UIClass.SetSettings(appInfo.Settings.GetConfig());
						appInfo.UIClass.SetCurrentDir(appInfo.AppDir);
						appInfo.UIClass.Init();
					}
					
					if( appInfo.ScriptClass != null )
					{
						appInfo.ScriptClass.SetAppInfo(appInfo);
						appInfo.ScriptClass.SetSettings(appInfo.Settings.GetConfig());
						appInfo.ScriptClass.SetCurrentDir(appInfo.AppDir);
						appInfo.ScriptClass.Init();
					}

					this.AddApplication(appInfo.AppID, appInfo);
				}
			}
		}

		/// <summary>
		/// Load the application informations
		/// </summary>
		/// <param name="DirName">Directory name containing the configuration file.</param>
		/// <param name="appInfo"><see cref="AppInfo"/> structure to be loaded.</param>
		/// <returns>True if valid configuration file found, false otherwise.</returns>
		private bool LoadAppSettings(string DirName, ref AppInfo appInfo)
		{
			AppConfig config = AppConfig.GetInstance(DirName);
			if( !config.Load() )
				return false;

			appInfo.Settings	= config;
			appInfo.AppID		= config.AppID;
			appInfo.Enabled		= config.Enabled;
			appInfo.AppDir		= config.AppDir;
			appInfo.AppName		= config.AppName;
			appInfo.AppDLL		= config.AppDLL;
			appInfo.AppIcon		= ImageHelper.LoadAppIcon(appInfo);

			/**
			 * Check for a valid AppID
			 */
			if( string.IsNullOrWhiteSpace(appInfo.AppID) || !StringHelper.IsValidID(appInfo.AppID) )
			{
				LoggerHelper.DebugMessage("Invalid App: " + appInfo.AppName);
				LoggerHelper.DebugMessage("App unique id is missing or is invalid (only a-zA-Z0-9_ characters allowed).");
				LoggerHelper.DebugMessage("App disabled.");
				appInfo.Enabled = false;
			}

			/**
			 * Check for a valid AppName
			 */
			if( string.IsNullOrWhiteSpace(appInfo.AppName) )
			{
				LoggerHelper.DebugMessage("Invalid App: " + appInfo.AppName);
				LoggerHelper.DebugMessage("App name is missing.");
				LoggerHelper.DebugMessage("App disabled.");
				appInfo.Enabled = false;
			}

			/**
			 * Check app compatibility
			 */
			if( !AssemblyHelper.CheckAssemblyVersion(config.MinApiVersion, config.MaxApiVersion) )
			{
				LoggerHelper.DebugMessage("Incompatible App found: " + appInfo.AppName);
				LoggerHelper.DebugMessage("CurrentVersion: " + AssemblyHelper.GetCurrentVersion());
				LoggerHelper.DebugMessage("MinVersion: " + config.MinApiVersion + ", MaxVersion: " + config.MaxApiVersion);
				LoggerHelper.DebugMessage("App disabled.");
				appInfo.Enabled = false;
			}

			return true;
		}

		internal void Scripts_MouseUp(object sender, GTA.MouseEventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach(AppInfo appInfo in appList)
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_MouseUp(sender, e);
			}
		}

		internal void Scripts_MouseDown(object sender, GTA.MouseEventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach( AppInfo appInfo in appList )
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_MouseDown(sender, e);
			}
		}

		internal void Scripts_PerFrameDrawing(object sender, GTA.GraphicsEventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach( AppInfo appInfo in appList )
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_PerFrameDrawing(sender, e);
			}
		}

		internal void Scripts_KeyUp(object sender, GTA.KeyEventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach( AppInfo appInfo in appList )
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_KeyUp(sender, e);
			}
		}

		internal void Scripts_KeyDown(object sender, GTA.KeyEventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach( AppInfo appInfo in appList )
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_KeyDown(sender, e);
			}
		}

		internal void Scripts_Tick(object sender, EventArgs e)
		{
			List<AppInfo> appList = this.GetAppScriptList();
			foreach( AppInfo appInfo in appList )
			{
				if( appInfo.ScriptClass != null )
					appInfo.ScriptClass.raise_Tick(sender, e);
			}
		}
	}
}
