﻿using LCPDDevice.Main;

namespace LCPDDevice.API.Managers
{
	/// <summary>
	/// Externalize some important functions of the device.
	/// </summary>
	public class DeviceManager
	{
		private static DeviceManager _instance;

		/// <summary>
		/// Return a valid instance of this class or create a new one if necessary.
		/// </summary>
		/// <returns><see cref="DeviceManager"/> valid instance.</returns>
		public static DeviceManager GetInstance()
		{
			if(_instance == null)
				_instance = new DeviceManager();

			return _instance;
		}

		private DeviceManager()
		{
			
		}

		/// <summary>
		/// Open/Close the device.
		/// </summary>
		public void ToggleDevice()
		{
			Device.GetInstance().Toggle();
		}

		/// <summary>
		/// Open the device.
		/// </summary>
		public void OpenDevice()
		{
			Device.GetInstance().Open();
		}

		/// <summary>
		/// Close the device.
		/// </summary>
		public void CloseDevice()
		{
			Device.GetInstance().Close();
		}
	}
}
