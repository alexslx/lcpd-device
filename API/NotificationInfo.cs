﻿using GTA;
using LCPDDevice.API.Managers;
using LCPDDevice.Main;

namespace LCPDDevice.API
{
	/// <summary>
	/// Object containing all information about a notification.
	/// </summary>
	/// <seealso cref="App"/>
	/// <seealso cref="AppManager"/>
	public class NotificationInfo
	{
		/// <summary>
		/// Notification unique id
		/// </summary>
		internal string Id;

		/// <summary>
		/// Application Name.
		/// </summary>
		internal string AppName;

		/// <summary>
		/// Application Icon.
		/// </summary>
		internal Texture AppIcon;

		/// <summary>
		/// String containing the notification message.
		/// </summary>
		public string Message;

		/// <summary>
		/// Duration time in milliseconds.
		/// </summary>
		public int Duration;

		/// <summary>
		/// Time when the notification will be deleted.
		/// </summary>
		internal long endTime;

		/// <summary>
		/// Create a notification structure.
		/// </summary>
		/// <param name="Message">String containing the message to be displayed.</param>
		/// <param name="Duration">Duration time in milliseconds.</param>
		public NotificationInfo(string Message, int Duration = 3000)
		{
			this.Message	= Message;
			this.Duration	= Duration;
			this.endTime	= -1L;
			this.AppIcon	= DeviceUI.GetDefaultIcon();
		}

		internal void SetAppName(string AppName)
		{
			this.AppName = AppName;
		}
	}
}
