﻿using LCPDDevice.Helpers;

namespace LCPDDevice.API.App
{
	/// <summary>
	/// Base application class used by both application interface and scripts.
	/// </summary>
	public abstract class AppBase
	{
		/// <summary>
		/// Current application directory.
		/// </summary>
		protected string CurrentDir;

		/// <summary>
		/// Current set of informations about this application.
		/// </summary>
		protected AppInfo AppInfo;

		/// <summary>
		/// Current instance of this application <see cref="IniFile">Settings Helper</see>.
		/// </summary>
		protected IniFile Settings;

		/// <summary>
		/// Application constructor
		/// </summary>
		public AppBase()
		{
		
		}

		/// <summary>
		/// Initialize the App
		/// </summary>
		public virtual void Init()
		{

		}

		/// <summary>
		/// Get the current application directory
		/// </summary>
		/// <returns>string containing the app directory</returns>
		public string GetCurrentDir()
		{
			return CurrentDir;
		}

		/// <summary>
		/// Get the current application <see cref="IniFile">Settings Helper</see>.
		/// </summary>
		/// <returns>the current settings helper.</returns>
		public IniFile GetSettings()
		{
			return this.Settings;
		}

		/// <summary>
		/// Get the current <see cref="AppInfo"/>
		/// </summary>
		internal AppInfo GetAppInfo()
		{
			return this.AppInfo;
		}

		/// <summary>
		/// Set the current <see cref="AppInfo"/>.
		/// </summary>
		/// <param name="info"></param>
		internal void SetAppInfo(AppInfo info)
		{
			this.AppInfo = info;
		}

		/// <summary>
		/// Set the current app directory. Used internally by LCPDDevice.
		/// </summary>
		/// <param name="path">String containing the app directory</param>
		internal void SetCurrentDir(string path)
		{
			this.CurrentDir = path;
		}

		/// <summary>
		/// Set the app <see cref="IniFile">Settings Helper</see>. Used Internally by LCPDDevice.
		/// </summary>
		/// <param name="settings"><see cref="IniFile">Settings Helper</see> instance.</param>
		internal void SetSettings(IniFile settings)
		{
			this.Settings = settings;
		}
	}
}
