﻿using System.Windows.Forms;
using GTA;

namespace LCPDDevice.API.App
{
	internal struct BoundKeyItem
	{
		public Keys Key;
		public KeyPressDelegate Delegate;
		public BoundKeyItem(Keys key, KeyPressDelegate deleg)
		{
			this.Key = key;
			this.Delegate = deleg;
		}
	}
}
