﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GTA.Forms;
using LCPDDevice.Main;

namespace LCPDDevice.API.App
{
	/// <summary>
	/// Base application UI class.
	/// This class is responsible of creating UI elements on LCPDDevice.
	/// </summary>
	/// <seealso cref="AppBase"/>
	public abstract class AppUI : AppBase
	{
		private List<Control> controlList;

		private Point offsetPoint;

		private Size size;

		/// <summary>
		/// Class constructor.
		/// </summary>
		public AppUI()
		{
			this.controlList = new List<Control>();
		}

		/// <summary>
		/// Get the screen <see cref="Size"/>.
		/// </summary>
		/// <returns>the screen size structure</returns>
		public Size GetSize()
		{
			return this.size;
		}

		/// <summary>
		/// Get the offset <see cref="Point"/> from the screen.
		/// </summary>
		/// <returns>the offset point from the screen</returns>
		public Point GetOffsetPoint()
		{
			return this.offsetPoint;
		}


		/// <summary>
		/// Set the current screen parameters to be used by applications.
		/// </summary>
		/// <param name="offsetPoint"><see cref="Point"/> where the screen is located.</param>
		/// <param name="size">Screen <see cref="Size"/>.</param>
		internal void SetScreenParameters(Point offsetPoint, Size size)
		{
			this.size = size;
			this.offsetPoint = offsetPoint;
		}

		/// <summary>
		/// Load the app components before display the app on the screen.
		/// </summary>
		public virtual void Load()
		{
			Window.GetInstance().SetOnFormClose(onFormClosed);
		}

		/// <summary>
		/// Display the application on the screen.
		/// Should be called from any class inheriting this base class and overriding this method.
		/// </summary>
		public virtual void Show()
		{
			CheckPositions();
			Window.GetInstance().Open();
		}

		/// <summary>
		/// Close the application and removes all controls from the form.
		/// Should be called from any class inheriting this base class and overriding this method.
		/// </summary>
		public virtual void Close()
		{
			RemoveAllControls();
			Window.GetInstance().Close();
		}

		/// <summary>
		/// Set screen background <see cref="Color"/>.
		/// </summary>
		/// <param name="color"><see cref="Color"/> structure.</param>
		public void SetFormColor(Color color)
		{
			Window.GetInstance().SetScreenColor(color);
		}

		/// <summary>
		/// Add a component to app.
		/// </summary>
		/// <param name="item">Control item to added.</param>
		public void AddControl(Control item)
		{
			item.Location = new Point(offsetPoint.X + item.Location.X, offsetPoint.Y + item.Location.Y);

			Window.GetInstance().AddControl(item);
			this.controlList.Add(item);
		}

		/// <summary>
		/// Add a list of components to app.
		/// </summary>
		/// <param name="list">A list of control elements to be added</param>
		public void AddControls(List<Control> list)
		{
			foreach( Control item in list )
				AddControl(item);
		}

		/// <summary>
		/// Get a component by its name.
		/// </summary>
		/// <param name="name">String containing the component name</param>
		/// <returns>The component control if found. Null otherwise.</returns>
		public Control GetControlByName(string name)
		{
			if( this.controlList == null )
				return null;

			foreach( Control item in this.controlList )
			{
				if( String.Equals(item.Name, name, StringComparison.Ordinal) )
					return item;
			}

			return null;
		}

		internal void SetAllControlsVisibility(bool isVisible = true)
		{
			if( this.controlList == null )
				return;

			foreach( Control item in this.controlList )
			{
				item.Visible = isVisible;
			}
		}

		private void onFormClosed(object sender, EventArgs e)
		{
			if( this.controlList == null )
				return;

			RemoveAllControls();
		}

		private void CheckPositions()
		{
			if( this.controlList == null )
				return;

			foreach( Control item in this.controlList )
			{
				// Check if the start X coordinate is out of the screen.
				if( item.Location.X < this.offsetPoint.X )
					item.Location = new Point(this.offsetPoint.X + item.Location.X, item.Location.Y);

				// Check if the start Y coordinate is out of the screen.
				if( item.Location.Y < this.offsetPoint.Y )
					item.Location = new Point(item.Location.X, this.offsetPoint.Y + item.Location.Y);

				// Check if the width is out of the screen.
				if( (item.Location.X + item.Width) >= (this.offsetPoint.X + this.size.Width) )
					item.Width = this.size.Width - (item.Location.X - this.offsetPoint.X) - 2;

				// Check if the height is out of the screen
				if( (item.Location.Y + item.Height) >= (this.offsetPoint.Y + this.size.Height) )
					item.Height = this.size.Height - (item.Location.Y - this.offsetPoint.Y) - 2;
			}
		}

		private void RemoveAllControls()
		{
			if( this.controlList == null )
				return;

			foreach( Control item in this.controlList )
			{
				try
				{
					Window.GetInstance().RemoveControl(item);
					item.Dispose();
				}
				catch( Exception )
				{
				}
			}

			this.controlList.Clear();
		}
	}
}
