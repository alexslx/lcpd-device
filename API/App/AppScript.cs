﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GTA;
using LCPDDevice.Helpers;

namespace LCPDDevice.API.App
{
	/// <summary>
	/// Base application class.
	/// This class should be inherited by any application that needs to run in background.
	/// This class is a simplified version of Script class of SHDN (As SHDN do not allow us to register new scripts on runtime).
	/// </summary>
	/// <seealso cref="AppBase"/>
	public abstract class AppScript : AppBase, IDisposable
	{

		private List<BoundKeyItem> keylist;

		/// <summary>
		/// All stored events will be executed every SHDN tick()
		/// </summary>
		protected event EventHandler Tick;

		/// <summary>
		/// All stored events will be executed when a key is pressed.
		/// </summary>
		protected event GTA.KeyEventHandler KeyDown;

		/// <summary>
		/// All stored events will be executed when a key is released.
		/// </summary>
		protected event GTA.KeyEventHandler KeyUp;

		/// <summary>
		/// All stored events will be executed when any mouse button is pressed.
		/// </summary>
		protected event GTA.MouseEventHandler MouseDown;

		/// <summary>
		/// All stored events will be executed when any mouse button is released.
		/// </summary>
		protected event GTA.MouseEventHandler MouseUp;

		/// <summary>
		/// All stored events will be executed EVERY frame.
		/// This should be used ONLY to draw things on the screen.
		/// NEVER CALL ANY WAIT OR BLOCKING METHOD HERE.
		/// </summary>
		protected event GraphicsEventHandler PerFrameDrawing;

		private bool disposed;

		/// <summary>
		/// Class constructor.
		/// </summary>
		public AppScript()
		{
			this.keylist = new List<BoundKeyItem>();
		}


		#region Key Binding

		/// <summary>
		/// Bind a key (including modifiers) combination to a specific method.
		/// </summary>
		/// <param name="Key">Key parameter</param>
		/// <param name="MethodToBindTo">Method to bind</param>
		protected void BindKey(Keys Key, KeyPressDelegate MethodToBindTo)
		{
			BoundKeyItem item;
			item.Key = Key;
			item.Delegate = MethodToBindTo;
			this.keylist.Add(item);
		}

		/// <summary>
		/// Bind a key (without modifiers) combination to a specific method.
		/// </summary>
		/// <param name="Key">Key parameter</param>
		/// <param name="Shift">Shift flag</param>
		/// <param name="Ctrl">Ctrl flag</param>
		/// <param name="Alt">Alt flag</param>
		/// <param name="MethodToBindTo">Method to bind</param>
		protected void BindKey(Keys Key, bool Shift, bool Ctrl, bool Alt, KeyPressDelegate MethodToBindTo)
		{
			Key &= Keys.KeyCode;
			if( Shift )
				Key |= Keys.Shift;
			if( Ctrl )
				Key |= Keys.Control;
			if( Alt )
				Key |= Keys.Alt;

			BoundKeyItem item;
			item.Key = Key;
			item.Delegate = MethodToBindTo;
			this.keylist.Add(item);
		}

		/// <summary>
		/// Unbind a key (including modifiers) combination.
		/// </summary>
		/// <param name="Key">Key parameter</param>
		protected void UnbindKey(Keys Key)
		{
			foreach(BoundKeyItem KeyItem in this.keylist)
			{
				if( KeyItem.Key == Key )
				{
					this.keylist.Remove(KeyItem);
				}
			}
		}

		/// <summary>
		/// Unbind a key (without modifiers) combination.
		/// </summary>
		/// <param name="Key">Key parameter</param>
		/// <param name="Shift">Shift flag</param>
		/// <param name="Ctrl">Ctrl flag</param>
		/// <param name="Alt">Alt flag</param>
		protected void UnbindKey(Keys Key, bool Shift, bool Ctrl, bool Alt)
		{
			Key &= Keys.KeyCode;
			if( Shift )
				Key |= Keys.Shift;
			if( Ctrl )
				Key |= Keys.Control;
			if( Alt )
				Key |= Keys.Alt;

			this.UnbindKey(Key);
		}

		private void ProcessBoundKey(Keys Key)
		{
			foreach(BoundKeyItem KeyItem in this.keylist)
			{
				if( KeyItem.Key == Key )
				{
					KeyItem.Delegate();
				}
			}
		}

		#endregion

		#region Event Raising

		internal void raise_Tick(object sender, EventArgs args)
		{
			EventHandler eventHandler = this.Tick;
			if (eventHandler != null)
			{
				eventHandler(sender, args);
			}
		}

		internal void raise_PerFrameDrawing(object sender, GraphicsEventArgs args)
		{
			GraphicsEventHandler graphicsEventHandler = this.PerFrameDrawing;
			if( graphicsEventHandler != null )
			{
				graphicsEventHandler(sender, args);
			}
		}

		internal void raise_KeyDown(object sender, GTA.KeyEventArgs args)
		{
			GTA.KeyEventHandler keyEventHandler = this.KeyDown;
			if (keyEventHandler != null)
			{
				keyEventHandler(sender, args);
			}

			if( this.keylist.Count > 0 )
			{
				this.ProcessBoundKey(args.KeyWithModifiers);
			}
		}

		internal void raise_KeyUp(object sender, GTA.KeyEventArgs args)
		{
			GTA.KeyEventHandler keyEventHandler = this.KeyUp;
			if (keyEventHandler != null)
			{
				keyEventHandler(sender, args);
			}
		}

		internal void raise_MouseDown(object sender, GTA.MouseEventArgs args)
		{
			GTA.MouseEventHandler mouseEventHandler = this.MouseDown;
			if (mouseEventHandler != null)
			{
				mouseEventHandler(sender, args);
			}
		}

		internal void raise_MouseUp(object sender, GTA.MouseEventArgs args)
		{
			GTA.MouseEventHandler mouseEventHandler = this.MouseUp;
			if (mouseEventHandler != null)
			{
				mouseEventHandler(sender, args);
			}
		}

		#endregion

		#region Destroying

		/// <summary>
		/// Disposing method.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Dispose executes in two distinct scenarios.
		/// If disposing equals true, the method has been called directly
		/// or indirectly by a user's code. Managed and unmanaged resources
		/// can be disposed.
		/// 
		/// If disposing equals false, the method has been called by the
		/// runtime from inside the finalizer and you should not reference
		/// other objects. Only unmanaged resources can be disposed.
		/// </summary>
		/// <param name="disposing">Flag to indicate whether the method has been called by a user's code or not.</param>
		internal virtual void Dispose(bool disposing)
		{
			if(!this.disposed)
			{
				if(disposing)
				{
				
				}

				this.disposed = true;
			}
		}

		/// <summary>
		/// Destroying method.
		/// </summary>
		~AppScript()
		{
			this.Dispose(false);
		}

		#endregion

	}
}
