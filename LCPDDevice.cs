﻿using System;
using System.Windows.Forms;
using GTA;
using LCPDDevice.API.Managers;
using LCPDDevice.Configs;
using LCPDDevice.Helpers;
using LCPDDevice.Main;

namespace LCPDDevice
{
    class LCPDDevice : Script
    {
		public const string APP_NAME  = "LCPDDevice";

		public const string BASE_PATH = "scripts/" + APP_NAME;
		public const string APPS_PATH = BASE_PATH + "/apps/";
		public const string SKIN_PATH = BASE_PATH + "/skin/";
		public const string LOGS_PATH = BASE_PATH + "/logs/";
		public const string DATA_PATH = BASE_PATH + "/data/";

		private DateTime engineStart;
		private bool eventsHooked;

		public LCPDDevice()
		{
			base.GUID = new Guid("8d78c42c-5828-4435-8905-0c90623deabe");
			base.BindConsoleCommand("device", new ConsoleCommandDelegate(this.OnCommandToggleDevice));

			try
			{
				Device.GetInstance(this).Load();
			}
			catch( Exception e )
			{
				DebugHelper.Instance.DebugMessage("FATAL ERROR: " + e.ToString());
			}

			this.engineStart	= DateTime.Now;
			this.eventsHooked	= false;

			BindToggleKeys();

			Tick				+= LCPDDevice_Tick;
		}

		private void BindToggleKeys()
		{
			bool Ctrl = false, Alt = false, Shift = false;

			DeviceConfig config = DeviceConfig.GetInstance();

			if( config.DeviceToggle == Keys.None )
				return;

			GetCtrlAltShift(config.DeviceToggleModifier, ref Ctrl, ref Alt, ref Shift);
			BindKey(config.DeviceToggle, Shift, Ctrl, Alt, ToggleDevice);
		}

		private void GetCtrlAltShift(Keys value, ref bool Ctrl, ref bool Alt, ref bool Shift)
		{
			if( value == Keys.Control )
				Ctrl = true;

			if( value == Keys.Alt )
				Alt = true;

			if( value == Keys.Shift )
				Shift = true;
		}

		private void OnCommandToggleDevice(ParameterCollection Parameter)
		{
			ToggleDevice();
		}

		private void ToggleDevice()
		{
			Device.GetInstance(this).Toggle();
		}

		private void LCPDDevice_Tick(object sender, EventArgs e)
		{
			// Start the scripts events 1 second later...
			if( (DateTime.Now - this.engineStart).TotalSeconds >= 1 )
			{
				AppManager.GetInstance().Scripts_Tick(sender, e);

				if( !this.eventsHooked )
				{
					this.eventsHooked	 = true;
					KeyDown				+= LCPDDevice_KeyDown;
					KeyUp				+= LCPDDevice_KeyUp;
					MouseDown			+= LCPDDevice_MouseDown;
					MouseUp				+= LCPDDevice_MouseUp;
					PerFrameDrawing		+= LCPDDevice_PerFrameDrawing;
				}
			}
		}

		private void LCPDDevice_PerFrameDrawing(object sender, GraphicsEventArgs e)
		{
			AppManager.GetInstance().Scripts_PerFrameDrawing(sender, e);
		}

		private void LCPDDevice_MouseUp(object sender, GTA.MouseEventArgs e)
		{
			AppManager.GetInstance().Scripts_MouseUp(sender, e);
		}

		private void LCPDDevice_MouseDown(object sender, GTA.MouseEventArgs e)
		{
			AppManager.GetInstance().Scripts_MouseDown(sender, e);
		}

		private void LCPDDevice_KeyUp(object sender, GTA.KeyEventArgs e)
		{
			AppManager.GetInstance().Scripts_KeyUp(sender, e);
		}

		private void LCPDDevice_KeyDown(object sender, GTA.KeyEventArgs e)
		{
			AppManager.GetInstance().Scripts_KeyDown(sender, e);
		}
    }
}
