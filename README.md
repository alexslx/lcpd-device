LCPD Device - GTAIV Modding
=========================================================
LCPD Device is a very customizable device interface for scripting inside of the GTA IV game.
Instead of remembering a large number of hotkeys, you can use it as a graphical interface to interact with your scripts on GTAIV.
Better yet, LCPD Device has been build in order to allow 3rd party developers to create new applications for it and extends its functionalities.


Requirements
------------------------------------
* The latest version of GTA IV or EFLC.
	* GTAIV - [1.0.7.0](https://support.rockstargames.com/hc/en-us/articles/200145406-Grand-Theft-Auto-IV-Patch-7-Title-Update-v-1-0-7-0-English-1-0-6-1-Russian-1-0-5-2-Japanese-).
	* EFLC - [1.1.2.0](https://support.rockstargames.com/hc/en-us/articles/200145386-Grand-Theft-Auto-Episodes-from-Liberty-City-Title-Update-1-1-2-0-Patch-2-).
* ScriptHookDotNet
	* [Download Page](http://hazardx.com/files/gta4_net_scripthook-83)
* .NET Framework 4.5
	* [Download Page](http://www.microsoft.com/en-us/download/details.aspx?id=30653)


	
Installation
------------------------------------
In order to start using the LCPD Device, you just need to follow the simple steps below:

- Download the "**LCPD Device - User Package**" from the Download section.
- Copy all contents from the package to the folder "**scripts**" inside of your __main__ GTAIV or EFLC folder (eg. D:\Games\Grand Theft Auto IV\GTAIV\scripts).



Third-Party Components
------------------------------------
Installing third party components is a very easy task, you need go to the "**LCPDDevice**" folder inside of your "**scripts**" folder (eg. D:\Games\Grand Theft Auto IV\GTAIV\scripts\LCPDDevice).
Therefore, you will see the following structure:

* __apps__: Here you can install, enable or disable third party applications. In order to install new apps, just copy the app folder to **this** folder.
* __logs__: Here you can find all logs created by installed applications.
* __skin__: Here you can find all installed skins. In order to install a new skin, just copy the skin folder to **this** folder and change the selected skin parameter on the main "**config.ini**" found on the "**LCPDDevice**" directory.
* __lang__: Here you can find all language strings displayed in game by the main application. Installed applications can use these strings or read their version of strings.



For Developers
------------------------------------
The main idea behind the LCPD Device has to create a easier way to display a graphical user interface for GTAIV scripts. Therefore, LCPD Device has extended some default functionalities from the ScriptHookDotNet and warped them in a easier way to use. For example, LCPD Device allows to your application load the screen layout from a XML file, allowing a quick edit for your needs.

Therefore, to build an application to be used within LCPD Device, there is a documentation file and an example project bundled with the developer package. In addition, there will be new API available in the next versions to come, based on both user and developer feedbacks.

Features:
* General app layout can be loaded from a XML file.
* Notification system allows you to display notifications to the user.
* Fully interaction with the ScriptHookDotNet API.


COPYING
------------------------------------
- Do not copy this script over any modding web site. If you want this mod to be displayed on your website, contact me for uploading so I can track and always keep them updated. Breaking this rule, will imply on a DCMA Takedown request to the website host company, and they always follow it. :)




Frequently Asked Questions (FAQ)
------------------------------------

- Your question here?
	- My Answer here :)

