﻿using LCPDDevice.Helpers;
using System.Windows.Forms;

namespace LCPDDevice.Configs
{
	class DeviceConfig
	{
		public bool Enabled		= false;
		public string SkinName	= "Default";
		public Keys DeviceToggle, DeviceToggleModifier;

		private static DeviceConfig _instance;
		public static DeviceConfig GetInstance()
		{
			if(_instance == null)
				_instance = new DeviceConfig();

			return _instance;
		}

		private DeviceConfig()
		{
			this.Load();
		}

		private void Load()
		{
			IniFile config	= new IniFile(LCPDDevice.BASE_PATH + "/config.ini");

			this.Enabled	= config.ReadBoolean("General", "Enabled", true);
			this.SkinName	= config.ReadString( "General",    "Skin", "Default");

			// Keybindings
			this.DeviceToggle			= config.ReadKey("Keys",         "DeviceToggle", Keys.None);
			this.DeviceToggleModifier	= config.ReadKey("Keys", "DeviceToggleModifier", Keys.None);
		}
	}
}
