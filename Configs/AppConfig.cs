﻿using System;
using System.Collections.Generic;
using System.IO;
using LCPDDevice.Helpers;

namespace LCPDDevice.Configs
{
	/// <summary>
	/// Handle the configurations of an App.
	/// </summary>
	public class AppConfig
	{
		/// <summary>
		/// Application Unique ID
		/// </summary>
		public string AppID;

		/// <summary>
		/// Application Name
		/// </summary>
		public string AppName;

		/// <summary>
		/// Application icon resource (filename).
		/// </summary>
		public string AppIcon;

		/// <summary>
		/// Application DLL file.
		/// </summary>
		public string AppDLL;

		/// <summary>
		/// Application Directory Path
		/// </summary>
		public string AppDir;

		/// <summary>
		/// Minimum API version to run this application.
		/// </summary>
		public string MinApiVersion;

		/// <summary>
		/// Maximum API version to run this application.
		/// </summary>
		public string MaxApiVersion;

		/// <summary>
		/// Define if the app is enabled or not.
		/// </summary>
		public bool Enabled;

		private IniFile config;

		private static Dictionary<string, AppConfig> appList = new Dictionary<string, AppConfig>();

		/// <summary>
		/// Get a valid instance of this class or create a new one if necessary.
		/// </summary>
		/// <param name="DirName">String containing the directory name of the App to be read.</param>
		/// <returns>AppConfig instance</returns>
		public static AppConfig GetInstance(string DirName)
		{
			if( !appList.ContainsKey(DirName) )
				appList[DirName] = new AppConfig(DirName);

			return appList[DirName];
		}

		private AppConfig(string DirName)
		{
			this.AppDir = LCPDDevice.APPS_PATH + DirName;
		}

		/// <summary>
		/// Return the <see cref="IniFile"/> instance.
		/// </summary>
		/// <returns><see cref="IniFile"/> instance.</returns>
		public IniFile GetConfig()
		{
			return this.config;
		}

		/// <summary>
		/// Load the configuration file if found.
		/// </summary>
		/// <returns>True when successful, false otherwise.</returns>
		public bool Load()
		{
			string filename = this.AppDir + "/config.ini";
			if( !File.Exists(filename) )
				return false;

			try
			{
				this.config		= new IniFile(this.AppDir + "/config.ini");
				this.AppID		= config.ReadString( "Application", "UniqueID", "");
				this.AppName	= config.ReadString( "Application",     "Name", "");
				this.AppIcon	= config.ReadString( "Application",     "Icon", "default_icon.png");
				this.AppDLL		= config.ReadString( "Application",      "DLL", "application.dll");
				this.Enabled	= config.ReadBoolean("Application",  "Enabled", false);

				this.MinApiVersion = config.ReadString("Application", "MinApiVersion", "X.X.X.X");
				this.MaxApiVersion = config.ReadString("Application", "MaxApiVersion", "X.X.X.X");

				return true;
			}
			catch( Exception ) {}

			return false;
		}
	}
}
