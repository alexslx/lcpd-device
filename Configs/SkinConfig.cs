﻿using System.Drawing;
using LCPDDevice.Helpers;

namespace LCPDDevice.Configs
{
	class SkinConfig
	{
		public string SkinName;
		public RectangleF NotifBar;
		public RectangleF Screen;
		public RectangleF HomeButton;

		private static SkinConfig _instance;
		public static SkinConfig GetInstance()
		{
			if(_instance == null)
				_instance = new SkinConfig();

			return _instance;
		}

		private SkinConfig()
		{
			//
		}

		public void Load(string SkinName)
		{
			this.SkinName	= SkinName;

			IniFile config	= new IniFile(LCPDDevice.SKIN_PATH + SkinName + "/config.ini");

			this.NotifBar	= ReadRectangle(config, "NotificationBar");
			this.Screen		= ReadRectangle(config,          "Screen");
			this.HomeButton = ReadRectangle(config,      "HomeButton");
		}

		private RectangleF ReadRectangle(IniFile config, string Section)
		{
			int x, y, w, h;
			x = config.ReadInteger(Section, "StartX", 0);
			y = config.ReadInteger(Section, "StartY", 0);
			w = config.ReadInteger(Section,  "Width", 0);
			h = config.ReadInteger(Section, "Height", 0);

			return new RectangleF(x, y, w, h);
		}
	}
}
