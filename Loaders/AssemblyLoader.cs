﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.Helpers;

namespace LCPDDevice.Loaders
{
	internal class AssemblyLoader
	{
		internal static bool LoadAppAssembly(ref AppInfo appInfo)
		{
			try
			{
				string filename = appInfo.AppDir + "/" + appInfo.AppDLL;

				if( File.Exists(filename) )
				{
					byte[] bytes = GTA.Helper.FileToData(filename);
					Assembly asb = Assembly.Load(bytes);

					// Load App and AppScript from the assembly.
					AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
					AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += CurrentDomain_ReflectionOnlyAssemblyResolve;

					IEnumerable<Type> appTypeList = asb.GetExportedTypes().Where(w => w.IsSubclassOf(typeof(AppUI)));
					IEnumerable<Type> scriptTypeList = asb.GetExportedTypes().Where(w => w.IsSubclassOf(typeof(AppScript)));

					AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve -= CurrentDomain_ReflectionOnlyAssemblyResolve;
					AppDomain.CurrentDomain.AssemblyResolve -= CurrentDomain_AssemblyResolve;

					Type appType = null;
					if( appTypeList.Count() > 0 )
						appType = appTypeList.FirstOrDefault();

					Type scriptType = null;
					if( scriptTypeList.Count() > 0 )
						scriptType = scriptTypeList.FirstOrDefault();

					if( appType != null )
					{
						appInfo.UIClass = (AppUI)Activator.CreateInstance(appType);

						if( appInfo.UIClass == null )
							return false;

						if( scriptType != null )
						{
							DebugHelper.Instance.DebugMessage("Found AppScript!");
							appInfo.ScriptClass = (AppScript)Activator.CreateInstance(scriptType);
						}

						return true;
					}

					return false;
				}
			}
			catch( Exception e )
			{
				DebugHelper.Instance.DebugUniqueMessage("LoadAppAssembly", "Exception: " + e.ToString());
			}

			return false;
		}

		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			if( args.Name.Contains(typeof(LCPDDevice).Assembly.GetName().Name) )
			{
				return Assembly.GetExecutingAssembly();
			}
			return null;
		}

		private static Assembly CurrentDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
		{
			Assembly asb = AppDomain.CurrentDomain.GetAssemblies().Where(w => w.FullName == args.Name).FirstOrDefault();
			return asb;
		}
	}
}
