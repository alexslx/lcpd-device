﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.XPath;
using LCPDDevice.API.Collections;
using LCPDDevice.Helpers;

namespace LCPDDevice.Loaders
{
	/// <summary>
	/// Loader class for collections.
	/// </summary>
	/// <seealso cref="GTACollection"/>
	/// <seealso cref="GTACollectionCategory"/>
	/// <seealso cref="GTACollectionItem"/>
	public class CollectionLoader
	{
		/// <summary>
		/// Load a entire collection from a file.
		/// </summary>
		/// <param name="filename">String containing the full file path to be loaded.</param>
		/// <returns>GTACollection object loaded from the file or null if any error occurs.</returns>
		public static GTACollection LoadCollectionFromFile(string filename)
		{
			if( !File.Exists(filename) )
				return null;

			GTACollection result = null;

			try
			{
				XPathDocument xmldoc = new XPathDocument(filename);
				XPathNavigator nav = xmldoc.CreateNavigator();

				if( nav.SelectSingleNode("collection/@id") == null )
					throw new NullReferenceException("Cannot load a collection without id.");

				string id = nav.SelectSingleNode("collection/@id").Value.ToUpper();
				result = new GTACollection();
				result.SetId(id);

				foreach( XPathNavigator node in nav.Select("collection/*") )
				{
					if( node.Name.Equals("item", StringComparison.OrdinalIgnoreCase) )
					{
						GTACollectionItem item = LoadItemNode(node);
						if( item != null )
							result.AddItem(item.GetId(), item);
					}

					if( node.Name.Equals("category", StringComparison.OrdinalIgnoreCase) )
					{
						GTACollectionCategory cat = LoadCategory(node);
						if( cat != null )
							result.AddCategory(cat.GetId(), cat);
					}
				}
			}
			catch( Exception e )
			{
				DebugHelper.Instance.DebugMessage("Error loading collection: " + e.ToString());
				return null;
			}

			return result;
		}

		private static GTACollectionCategory LoadCategory(XPathNavigator node)
		{
			try
			{
				GTACollectionCategory cat = new GTACollectionCategory();

				if( node.SelectSingleNode("@id") == null )
					throw new NullReferenceException("Cannot load a category without id.");

				string id	= node.SelectSingleNode("@id").Value.ToUpper();
				string name = node.SelectSingleNode("@name") != null ? node.SelectSingleNode("@name").Value : id;
				cat.SetId(id);
				cat.SetCategoryName(name);

				if( string.IsNullOrWhiteSpace(id) || id.Equals("UNNAMED", StringComparison.OrdinalIgnoreCase) )
					throw new ArgumentException("Category unique id cannot be blank or named 'unnamed'.");

				foreach( XPathNavigator itemnode in node.Select("*") )
				{
					if( itemnode.Name.Equals("item", StringComparison.OrdinalIgnoreCase) )
					{
						GTACollectionItem item = LoadItemNode(itemnode);
						if( item != null )
							cat.AddItem(item.GetId(), item);
					}
				}

				return cat;
			}
			catch( Exception e )
			{
				LoggerHelper.DebugMessage("Error loading collection category: " + e.ToString());
				return null;
			}
		}

		private static GTACollectionItem LoadItemNode(XPathNavigator node)
		{
			try
			{
				GTACollectionItem item = new GTACollectionItem();

				if( node.SelectSingleNode("@id") == null )
					throw new NullReferenceException("Cannot load a item without id.");

				string id = node.SelectSingleNode("@id").Value.ToUpper();
				item.SetId(id);

				if( node.SelectSingleNode("@value") != null )
					item.SetValue(node.SelectSingleNode("@value").Value);

				foreach( XPathNavigator paramnode in node.Select("*") )
				{
					if( paramnode.SelectSingleNode("@id") == null || paramnode.SelectSingleNode("@value") == null )
					{
						DebugHelper.Instance.DebugMessage("Error loading collection item parameters: No id or value has been defined.");
						continue;
					}

					string paramid = paramnode.SelectSingleNode("@id").Value.ToUpper();
					string value = paramnode.SelectSingleNode("@value").Value;

					if( string.IsNullOrWhiteSpace(paramid) )
					{
						DebugHelper.Instance.DebugMessage("Error loading collection item parameters: id cannot be blank.");
						continue;
					}

					if( paramid.Equals("EPISODE", StringComparison.OrdinalIgnoreCase) )
					{
						if( value.Equals("TLAD", StringComparison.OrdinalIgnoreCase) && GTA.Game.CurrentEpisode != GTA.GameEpisode.TLAD )
							continue;

						if( value.Equals("TBOGT", StringComparison.OrdinalIgnoreCase) && GTA.Game.CurrentEpisode != GTA.GameEpisode.TBOGT )
							continue;

						if( value.Contains("TLAD") && value.Contains("TBOGT") &&
							(GTA.Game.CurrentEpisode != GTA.GameEpisode.TLAD || GTA.Game.CurrentEpisode != GTA.GameEpisode.TBOGT)
							)
							continue;
					}

					item.AddParam(paramid, value);
				}

				return item;
			}
			catch( Exception e )
			{
				DebugHelper.Instance.DebugMessage("Error loading collection item " + e.ToString());
				return null;
			}
		}
	}
}
