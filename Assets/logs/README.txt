LCPD Device - Logs
==============================
LCPD Device applications might place important logs here. It is useful when you need to report application bugs to the author.
