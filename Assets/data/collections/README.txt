LCPD Device - Collections
==============================
LCPD Device will automatically load all *.xml files located here and all applications have access to the loaded collections. Therefore, common useful lists will be updated and placed here.

Please, if you find any error to the lists contained here, please report to us!