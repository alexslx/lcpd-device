﻿using System.Collections.Generic;
using System.Drawing;
using GTA;
using GTA.Forms;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.API.Managers;

namespace LCPDDevice.Applications
{
	class Homescreen : AppUI
	{
		private const int START_X = 1;
		private const int START_Y = 0;

		private const int ICON_WIDTH  = 48;
		private const int ICON_HEIGHT = 48;
		private const int ICON_MARGIN = 8;

		public Homescreen()
		{
			
		}

		public override void Load()
		{
			base.Load();

			this.SetFormColor(Color.Yellow);

			int px = START_X, py = START_Y;
			List<AppInfo> appList = AppManager.GetInstance().GetAppList();

			foreach( AppInfo app in appList )
			{
				this.CreateIcon(app, new Point(px, py));

				px += ICON_WIDTH + ICON_MARGIN;
				if( this.GetSize().Width <= (px + ICON_WIDTH) )
				{
					px = START_X;
					py += ICON_HEIGHT + ICON_MARGIN;
				}
			}
		}

		private void CreateIcon(AppInfo app, Point point)
		{
			Imagebox appImgBox			= new Imagebox();
			appImgBox.BackColor			= Color.Transparent;
			appImgBox.Visible			= true;
			appImgBox.Width				= ICON_WIDTH;
			appImgBox.Height			= ICON_HEIGHT;
			appImgBox.Location			= point;
			appImgBox.Image				= app.AppIcon;
			appImgBox.Name				= app.AppID;
			appImgBox.Text				= app.AppName;
			appImgBox.Click			   += OnIconClick;
			this.AddControl(appImgBox);
		}

		private void OnIconClick(object sender, MouseEventArgs e)
		{
			Imagebox appImgBox = (Imagebox)sender;
			AppManager.GetInstance().LaunchApp(appImgBox.Name);
		}
	}
}
