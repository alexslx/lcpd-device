﻿using System;
using System.Drawing;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.API.Managers;
using LCPDDevice.Applications;
using LCPDDevice.Configs;

namespace LCPDDevice.Main
{
	class Device
	{
		private bool isOpen			= false;
		private bool isLoaded		= false;

		private LCPDDevice basescript;

		private AppUI currentApp;

		private Point offsetPoint;

		// Singleton
		private static Device _instance;
		public static Device GetInstance(LCPDDevice basescript)
		{
			if(_instance == null)
				_instance = new Device(basescript);

			return _instance;
		}

		public static Device GetInstance()
		{
			if( _instance == null )
				throw new NullReferenceException();

			return _instance;
		}

		private Device(LCPDDevice basescript)
		{
			this.basescript = basescript;
			NotificationManager.GetInstance().RegisterDrawer(basescript);
		}

		public void Load()
		{
			DeviceUI.GetInstance(this).Load(DeviceConfig.GetInstance().SkinName);
			GTACollectionManager.GetInstance().Load();
			AppManager.GetInstance().Load();
		}

		private void SetOpen(bool v)
		{
			this.isOpen = v;
		}

		public bool IsOpen()
		{
			return this.isOpen;
		}

		private void Create()
		{
			DeviceUI.GetInstance(this).RegisterDrawer(this.basescript);

			Point devicePoint	= DeviceUI.GetInstance(this).GetDevicePoint();
			Point screenPoint	= DeviceUI.GetInstance(this).GetScreenOffset();

			this.offsetPoint	= screenPoint;

			this.currentApp = new Homescreen();
		}

		public LCPDDevice GetBaseScript()
		{
			return this.basescript;
		}

		public void Toggle()
		{
			if( this.IsOpen() )
				this.Close();
			else
				this.Open();
		}

		public void Open()
		{
			this.isOpen = true;

			if( !this.isLoaded )
			{
				this.Create();
				this.isLoaded = true;
			}

			if( this.currentApp != null )
			{
				this.currentApp.SetScreenParameters(offsetPoint, DeviceUI.GetInstance(this).GetScreenSize());
				this.currentApp.Load();
				this.currentApp.Show();
			}
		}

		public void Close()
		{
			this.isOpen = false;
			
			if( this.currentApp != null )
				this.currentApp.Close();
		}

		public void SetCurrentApp(AppUI app)
		{
			if( this.currentApp != null )
			{
				this.currentApp.Close();
				this.currentApp = null;
			}

			this.currentApp = app;
		}

		public void ToggleNotificationBar(bool isNotificationOpen)
		{
			if( this.currentApp != null )
				this.currentApp.SetAllControlsVisibility(!isNotificationOpen);
		}
	}
}
