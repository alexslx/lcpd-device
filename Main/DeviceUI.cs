﻿using System;
using System.Drawing;
using System.IO;
using GTA;
using LCPDDevice.Configs;
using LCPDDevice.Helpers;

namespace LCPDDevice.Main
{
	class DeviceUI
	{
		private Texture devTexture;
		private RectangleF deviceRect;
		private RectangleF screenRect;
		private RectangleF notifRect;
		private RectangleF homeBtnRect;

		private Device tablet;
		private static byte[] deviceBytes;
		private static Texture defaultIcon;

		public string SkinName;

		// Singleton
		private static DeviceUI _instance;
		public static DeviceUI GetInstance(Device tablet)
		{
			if( _instance == null )
				_instance = new DeviceUI(tablet);

			return _instance;
		}

		public static DeviceUI GetInstance()
		{
			if( _instance == null )
				throw new NullReferenceException();

			return _instance;
		}

		private DeviceUI(Device tablet)
		{
			this.tablet	= tablet;
		}

		public void Load(string SkinName)
		{
			string skinfile			= LCPDDevice.SKIN_PATH + SkinName + "/device.png";
			string iconfile			= LCPDDevice.SKIN_PATH + SkinName + "/default_icon.png";

			if( !File.Exists(skinfile) )
				throw new FileNotFoundException("Skin error: device.png not found.");

			if( !File.Exists(iconfile) )
				throw new FileNotFoundException("Skin error: default_icon.png not found.");

			defaultIcon				= new Texture(Helper.FileToData(iconfile));
			deviceBytes				= Helper.FileToData(skinfile);
			Bitmap image			= ImageHelper.BytesToBitmap(deviceBytes);
			GraphicsUnit units		= GraphicsUnit.Pixel;
			this.deviceRect			= image.GetBounds(ref units);
			this.devTexture			= new Texture(deviceBytes);

			SkinConfig.GetInstance().Load(SkinName);
			this.screenRect			= SkinConfig.GetInstance().Screen;
			this.notifRect			= SkinConfig.GetInstance().NotifBar;
			this.homeBtnRect		= SkinConfig.GetInstance().HomeButton;
			this.SkinName			= SkinName;

			image.Dispose();
		}

		public void RegisterDrawer(LCPDDevice basescript)
		{
			basescript.PerFrameDrawing += new GraphicsEventHandler(this.DrawDevice);
		}

		public void DrawDevice(object sender, GraphicsEventArgs gdx)
		{
			if( !tablet.IsOpen() )
				return;

			Point p = this.GetDevicePoint();
			gdx.Graphics.DrawSprite(this.devTexture, new RectangleF(p.X, p.Y, this.deviceRect.Width, this.deviceRect.Height));
		}

		public Point GetDevicePoint()
		{
			int x = (int)(Game.Resolution.Width / 2 - this.deviceRect.Width / 2);
			int y = (int)(Game.Resolution.Height / 2 - this.deviceRect.Height / 2);

			return new Point(x, y);
		}

		public Size GetDeviceSize()
		{
			return new Size((int)this.deviceRect.Width, (int)this.deviceRect.Height);
		}

		public RectangleF GetDevice()
		{
			return this.deviceRect;
		}

		public Size GetScreenSize()
		{
			return new Size((int)this.screenRect.Width, (int)this.screenRect.Height);
		}

		public Point GetScreenOffset()
		{
			return new Point((int)this.screenRect.X, (int)this.screenRect.Y);
		}

		public RectangleF GetScreen()
		{
			return this.screenRect;
		}

		public Size GetNotificationSize()
		{
			return new Size((int)this.notifRect.Width, (int)this.notifRect.Height);
		}

		public Point GetNotificationOffset()
		{
			return new Point((int)this.notifRect.X, (int)this.notifRect.Y);
		}

		public RectangleF GetNotificationBar()
		{
			return this.notifRect;
		}

		public Size GetHomeBtnSize()
		{
			return new Size((int)this.homeBtnRect.Width, (int)this.homeBtnRect.Height);
		}

		public Point GetHomeBtnOffset()
		{
			return new Point((int)this.homeBtnRect.X, (int)this.homeBtnRect.Y);
		}

		public RectangleF GetHomeButton()
		{
			return this.homeBtnRect;
		}

		public static Texture GetDefaultIcon()
		{
			return defaultIcon;
		}
	}
}
