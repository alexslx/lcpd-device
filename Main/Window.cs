﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GTA.Forms;
using LCPDDevice.API;
using LCPDDevice.API.Managers;
using LCPDDevice.Configs;

namespace LCPDDevice.Main
{
	class Window
	{
		private Form form;

		private bool isNotificationOpen;

		private Color screenColor;

		private static GTA.Font notifiFont;

		//TODO: Find a better way to fix form opening/closing.
		private static bool fixClose;

		private const int NOTIFICATIONBAR_CLOSE_HEIGHT = 20;

		// Singleton
		private static Window _instance;
		public static Window GetInstance()
		{
			if( _instance == null )
				_instance = new Window();

			return _instance;
		}

		private Window()
		{
			this.form				= new Form();
			this.form.BackColor		= Color.FromArgb(0, 0, 0, 0);
			this.form.Size			= DeviceUI.GetInstance().GetDeviceSize();
			this.form.TitleSize		= 0;
			this.form.StartPosition = GTA.FormStartPosition.Fixed;
			this.form.Click		   += OnClick;
			this.form.Paint		   += OnPaint;

			this.screenColor		= Color.FromArgb(230, Color.Black);

			notifiFont				= new GTA.Font(16f, GTA.FontScaling.Pixel);
		}

		~Window()
		{
			if( notifiFont != null )
				notifiFont.Dispose();

			notifiFont = null;
		}

		public void Open()
		{
			fixClose = true;
			this.form.Location = DeviceUI.GetInstance().GetDevicePoint();
			this.form.KeyUp   += OnKeyUp;
			this.form.Show();
		}

		public void Close()
		{
			this.form.KeyUp   -= OnKeyUp;
			this.form.Close();
		}

		public void AddControl(Control item)
		{
			this.form.Controls.Add(item);
		}

		public bool RemoveControl(Control item)
		{
			return this.form.Controls.Remove(item);
		}

		public void SetScreenColor(Color color)
		{
			this.screenColor = color;
		}

		public void SetOnFormClose(EventHandler handler)
		{
			this.form.Closed += handler;
		}

		private void OnKeyUp(object sender, GTA.KeyEventArgs e)
		{
			if( fixClose )
			{
				fixClose = false;
				return;
			}

			System.Windows.Forms.Keys key			= DeviceConfig.GetInstance().DeviceToggle;
			System.Windows.Forms.Keys keyModifier	= DeviceConfig.GetInstance().DeviceToggleModifier;

			if( e.KeyWithModifiers == (key | keyModifier) )
				Device.GetInstance().Close();
		}

		private void OnClick(object sender, GTA.MouseEventArgs e)
		{
			DeviceUI deviceUI			= DeviceUI.GetInstance();
			RectangleF device			= deviceUI.GetDevice();
			RectangleF screen			= deviceUI.GetScreen();
			RectangleF notificationBar	= deviceUI.GetNotificationBar();
			RectangleF homeButton		= deviceUI.GetHomeButton();


			int X = e.PixelLocation.X - deviceUI.GetDevicePoint().X;
			int Y = e.PixelLocation.Y - deviceUI.GetDevicePoint().Y;
			Point P = new Point(X, Y);

			if( homeButton.Contains(X, Y) )
				OnHomeButtonClick(P);

			if( notificationBar.Contains(X, Y) )
				OnNotificationBarClick(P);

			if( screen.Contains(X, Y) )
				OnScreenClick(P);
		}

		private void OnScreenClick(Point P)
		{
			if( isNotificationOpen )
			{
				isNotificationOpen = false;
				Device.GetInstance().ToggleNotificationBar(isNotificationOpen);
				return;
			}
		}

		private void OnNotificationBarClick(Point P)
		{
			if(!isNotificationOpen)
			{
				isNotificationOpen = true;
				Device.GetInstance().ToggleNotificationBar(isNotificationOpen);
				return;
			}
		}

		private void OnHomeButtonClick(Point P)
		{
			AppManager.GetInstance().GoToHome();
		}

		private void OnPaint(object sender, GTA.GraphicsEventArgs e)
		{
			float X, Y;
			string currentDate		= GTA.World.CurrentDate.DayOfWeek.ToString().Substring(0, 3).ToUpper();
			string currentDateTime	= string.Format("{0} {1:D2}:{2:D2}", currentDate, GTA.World.CurrentDate.Hour, GTA.World.CurrentDate.Minute);

			DeviceUI deviceUI			= DeviceUI.GetInstance();
			RectangleF device			= deviceUI.GetDevice();
			RectangleF screen			= deviceUI.GetScreen();
			RectangleF notificationBar	= deviceUI.GetNotificationBar();

			// Notification Manager
			NotificationManager notifManager = NotificationManager.GetInstance();
			List<NotificationInfo> notifList = notifManager.GetNotificationList();

			// Notification Bar
			X = deviceUI.GetDevicePoint().X + notificationBar.X;
			Y = deviceUI.GetDevicePoint().Y + notificationBar.Y;
			RectangleF notifRect	= new RectangleF(X, Y, notificationBar.Width, notificationBar.Height);

			// Draw the bar background
			e.Graphics.DrawRectangle(notifRect, Color.Black);

			// Draw the clock
			notifRect.Y      += 2f;
			notifRect.Size   -= new Size(5, 2);
			e.Graphics.DrawText(currentDateTime, notifRect, GTA.TextAlignment.Right, Color.White, notifiFont);

			// If there is a notification to be displayed, show it.
			if(notifList.Count > 0)
			{
				NotificationInfo ni = notifList[0];
				int alpha = notifManager.GetNotificationAlpha(ni);
				
				notifRect.X += 5f;
				notifRect.Size -= new Size(5, 0);
				e.Graphics.DrawText(ni.Message, notifRect, GTA.TextAlignment.Left, Color.FromArgb(alpha, Color.White), notifiFont);
			}

			// Screen area
			X = deviceUI.GetDevicePoint().X + screen.X;
			Y = deviceUI.GetDevicePoint().Y + screen.Y;
			RectangleF screenRect = new RectangleF(X, Y, screen.Width, screen.Height);
			if(isNotificationOpen)
			{
				int count = ((int)screenRect.Height - NOTIFICATIONBAR_CLOSE_HEIGHT) / NotificationManager.NOTIFICATION_HEIGHT;
				e.Graphics.DrawRectangle(screenRect, Color.FromArgb(230, Color.Black));

				float NY = screenRect.Y;
				for(int i=0; i < Math.Min(count, notifList.Count); i++)
				{
					NotificationInfo notifInfo = notifList[i];
					notifManager.DrawNotification(notifInfo, e, X, NY, screen.Width, NotificationManager.NOTIFICATION_HEIGHT, true);
				}

				RectangleF closeNotifRect = new RectangleF(X, (Y + screen.Height - NOTIFICATIONBAR_CLOSE_HEIGHT), screen.Width, NOTIFICATIONBAR_CLOSE_HEIGHT);
				e.Graphics.DrawRectangle(closeNotifRect, Color.Black);

				closeNotifRect.Y += 2f;
				e.Graphics.DrawText("[ CLOSE NOTIFICATION BAR ]", closeNotifRect, GTA.TextAlignment.Center, Color.White, notifiFont);
			}
			else
			{
				e.Graphics.DrawRectangle(screenRect, this.screenColor);
			}
		}
	}
}
