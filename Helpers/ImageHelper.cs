﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using LCPDDevice.API;
using LCPDDevice.Main;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// Helper file to image data's.
	/// </summary>
	public class ImageHelper
	{
		/// <summary>
		/// Convert a <see cref="byte"/> array to a <see cref="Bitmap"/>.
		/// </summary>
		/// <param name="byteArray">Array of <see cref="byte"/>.</param>
		/// <returns><see cref="Bitmap"/> descriptor.</returns>
		public static Bitmap BytesToBitmap(byte[] byteArray)
		{
			TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));

			return (Bitmap)tc.ConvertFrom(byteArray);
		}

		/// <summary>
		/// Load the application icon or use the default for non-existent ones.
		/// </summary>
		/// <param name="appInfo"><see cref="AppInfo"/> structure containing the required information.</param>
		/// <returns>Loaded or default texture.</returns>
		public static GTA.Texture LoadAppIcon(AppInfo appInfo)
		{
			string filename = appInfo.AppDir + "/" + appInfo.Settings.AppIcon;
			if( File.Exists(filename) )
			{
				byte[] iconByte = GTA.Helper.FileToData(filename);
				return new GTA.Texture(iconByte);
			}

			return DeviceUI.GetDefaultIcon();
		}
	}
}
