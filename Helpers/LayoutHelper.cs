﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.XPath;
using GTA.Forms;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// Helper class to convert a XML file to a list of valid Controls.
	/// </summary>
	public class LayoutHelper
	{
		/// <summary>
		/// Load a XML file and return a list of valid Controls.
		/// </summary>
		/// <param name="filename">String containing the XML filename to be loaded.</param>
		/// <returns>A list of valid control elements loaded from the XML file.</returns>
		public static List<Control> LoadLayout(string filename)
		{
			List<Control> elements  = new List<Control>();
			XPathDocument xmldoc	= new XPathDocument(filename);
			XPathNavigator nav		= xmldoc.CreateNavigator();

			foreach (XPathNavigator node in nav.Select("layout/*"))
			{
				Control c = null;
				switch( node.Name )
				{
					case "Button":		c = ReadButton(node); 			break;
					case "CheckBox":	c = ReadCheckBox(node); 		break;
					case "ImageBox":	c = ReadImageBox(node); 		break;
					case "KeyBox":		c = ReadKeyBox(node); 			break;
					case "Label":		c = ReadLabel(node); 			break;
					case "ListBox":		c = ReadListBox(node); 			break;
					case "TextBox":		c = ReadTextBox(node); 			break;
					default: break;
				}
				if( c != null )
					elements.Add(c);
			}

			return elements;
		}

		private static void ReadAndSetControl(Control c, XPathNavigator node)
		{
			foreach (XPathNavigator subnode in node.Select("*"))
			{
				switch( subnode.Name )
				{
					case "Name":				c.Name				= subnode.Value;									break;
					case "Text":				c.Text				= subnode.Value;									break;
					case "Visible":				c.Visible			= StringHelper.ReadSafeBool(subnode.Value, true);	break;
					case "Transparency":		c.Transparency		= StringHelper.ReadSafeFloat(subnode.Value, 1f);	break;
					case "Width":				c.Width				= StringHelper.ReadSafeInt(subnode.Value, 100);		break;
					case "Height":				c.Height			= StringHelper.ReadSafeInt(subnode.Value, 100);		break;
					/*case "Font":				item.Font				= subnode.Value;		break;*/ // TODO: Implement
					case "Location":			c.Location			= StringHelper.ReadSafePoint(subnode.Value, new Point(0,0));		break;
					case "Size":				c.Size				= StringHelper.ReadSafeSize(subnode.Value, new Size(64,64));		break;
					case "BackColor":			c.BackColor			= StringHelper.ReadSafeColor(subnode.Value, Color.FromArgb(  0, 200, 200, 200));	break;
					case "ForeColor":			c.ForeColor			= StringHelper.ReadSafeColor(subnode.Value, Color.FromArgb(255, 255, 255, 255));	break;
					default: break;
				}
			}
		}

		private static Textbox ReadTextBox(XPathNavigator node)
		{
			Textbox e = new Textbox();
			ReadAndSetControl(e, node);

			foreach (XPathNavigator subnode in node.Select("*"))
			{
				switch( subnode.Name )
				{
					case "MaxLength":	e.MaxLength = StringHelper.ReadSafeInt(subnode.Value, 100);		break;
					case "Border":		e.Border	= StringHelper.ReadSafeBool(subnode.Value, true);	break;
					default: break;
				}
			}

			return e;
		}

		private static Listbox ReadListBox(XPathNavigator node)
		{
			Listbox e = new Listbox();
			ReadAndSetControl(e, node);

			foreach (XPathNavigator subnode in node.Select("*"))
			{
				switch( subnode.Name )
				{
					case "SelectionColor":		e.SelectionColor	= StringHelper.ReadSafeColor(subnode.Value, Color.FromArgb(127, 48, 64, 192));		break;
					case "ScrollbarSize":		e.ScrollbarSize		= StringHelper.ReadSafeInt(subnode.Value, 16);										break;
					case "Border":				e.Border			= StringHelper.ReadSafeBool(subnode.Value, true);									break;
					default: break;
				}
			}

			return e;
		}

		private static Label ReadLabel(XPathNavigator node)
		{
			Label e = new Label();
			ReadAndSetControl(e, node);

			return e;
		}

		private static Keybox ReadKeyBox(XPathNavigator node)
		{
			Keybox e = new Keybox();
			ReadAndSetControl(e, node);

			foreach (XPathNavigator subnode in node.Select("*"))
			{
				switch( subnode.Name )
				{
					case "SelectedKey":
						e.SelectedKey = StringHelper.ReadSafeKey(subnode.Value, System.Windows.Forms.Keys.None);
					break;

					default: break;
				}
			}

			return e;
		}

		private static Imagebox ReadImageBox(XPathNavigator node)
		{
			Imagebox e = new Imagebox();
			ReadAndSetControl(e, node);

			foreach( XPathNavigator subnode in node.Select("*") )
			{
				switch( subnode.Name )
				{
					case "Border":
					{
						GTA.BorderType result;
						try
						{
							if( Enum.TryParse(subnode.Value, out result) )
								e.Border = result;
						}
						catch(Exception)
						{
						
						}
					}
					break;

					case "Texture":
					{
						// TODO: Implement
					}
					break;

					default:
					break;
				}
			}

			return e;
		}

		private static Checkbox ReadCheckBox(XPathNavigator node)
		{
			Checkbox e = new Checkbox();
			ReadAndSetControl(e, node);

			foreach (XPathNavigator subnode in node.Select("*"))
			{
				switch( subnode.Name )
				{
					case "CheckColor":			e.CheckColor		= StringHelper.ReadSafeColor(subnode.Value, Color.FromArgb(240, 48, 64, 192));		break;
					case "Checked":				e.Checked			= StringHelper.ReadSafeBool(subnode.Value, true);									break;
					default: break;
				}
			}

			return e;
		}

		private static Button ReadButton(XPathNavigator node)
		{
			Button e = new Button();
			ReadAndSetControl(e, node);

			return e;
		}


	}
}
