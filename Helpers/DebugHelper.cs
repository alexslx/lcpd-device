﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using GTA;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// General debugging helper.
	/// </summary>
	internal class DebugHelper
	{
#if DEBUG
		private StreamWriter logWriter = null;
		private NameValueCollection debugWatcher = new NameValueCollection();
		// Debug message watcher
#endif
		// Singleton instance
		private static readonly Lazy<DebugHelper> _instance = new Lazy<DebugHelper>(() => new DebugHelper());
		private DebugHelper()
		{
#if DEBUG
			try
			{
				logWriter = File.CreateText(LCPDDevice.LOGS_PATH + LCPDDevice.APP_NAME + ".log");
			}
			catch( Exception )
			{
			}
#endif
		}

		/// <summary>
		/// Proper destroy the class.
		/// </summary>
		~DebugHelper()
		{
#if DEBUG
			try
			{
				if( logWriter != null )
				{
					logWriter.Flush();
					logWriter.Close();
				}

			}
			catch( Exception )
			{
			}
#endif
		}

		/// <summary>
		/// Get a valid instance of this class.
		/// </summary>
		public static DebugHelper Instance
		{
			get
			{
				return _instance.Value;
			}
		}

		/// <summary>
		/// Print a debug message if not equal of the last one with the same unique Id.
		/// Useful for statuses or printing inside loops to avoid console overflow from the same message.
		/// The message is also saved to the log file.
		/// </summary>
		/// <param name="uniqueId">String containing the unique Id to track changes.</param>
		/// <param name="message">String containing the message to be logged.</param>
		[ConditionalAttribute("DEBUG")]
		public void DebugUniqueMessage(string uniqueId, string message)
		{
#if DEBUG
			if( this.debugWatcher[uniqueId] != null && this.debugWatcher[uniqueId] == message )
				return;

			this.debugWatcher[uniqueId] = message;

			DebugMessage(message);
#endif
		}

		/// <summary>
		/// Print a debug message on the console and save it to the log file.
		/// </summary>
		/// <param name="msg">String containing the message to be saved and displayed.</param>
		[ConditionalAttribute("DEBUG")]
		public void DebugMessage(string msg)
		{
#if DEBUG
			string message = string.Format("[{0} - {1}] {2}", LCPDDevice.APP_NAME, DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss:ffff"), msg);

			Game.Console.Print(message);

			WriteLog(message);
#endif
		}

		/// <summary>
		/// Write the message on the log file
		/// </summary>
		/// <param name="message">String containing the message.</param>
		[ConditionalAttribute("DEBUG")]
		private void WriteLog(string message)
		{
#if DEBUG
			try
			{
				if( logWriter != null )
					logWriter.WriteLine(message);
			}
			catch( Exception e )
			{
				Game.Console.Print("Exception while writing log file: " + e.Message);
			}
#endif
		}
	}
}
