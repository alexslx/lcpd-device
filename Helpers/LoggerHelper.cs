﻿using System;
using System.Collections.Specialized;
using System.IO;
using GTA;
using LCPDDevice.API;
using LCPDDevice.API.App;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// General Logging Helper.
	/// </summary>
	public class LoggerHelper
	{
		private static NameValueCollection debugWatcher = new NameValueCollection();

		private static void WriteLogFile(string filename, string message)
		{
			if( string.IsNullOrWhiteSpace(filename) )
				return;

			try
			{
				filename = filename.Trim().Replace(' ', '_');
				filename = filename.ToLowerInvariant();

				StreamWriter logWriter	= File.CreateText(LCPDDevice.LOGS_PATH + filename + ".log");

				logWriter.WriteLine(message);
				logWriter.Flush();
				logWriter.Close();
			}
			catch(Exception e)
			{
				DebugHelper.Instance.DebugMessage("Cannot write appInfo log file: " + e.ToString());
			}
		}

		/// <summary>
		/// Print a debug message if not equal of the last one with the same unique Id.
		/// Useful for statuses or printing inside loops to avoid console overflow from the same message.
		/// The message is also saved to the log file.
		/// </summary>
		/// <param name="app">Current application instance.</param>
		/// <param name="uniqueId">String containing the unique Id to track changes.</param>
		/// <param name="message">String containing the message to be logged.</param>
		public static void DebugUniqueMessage(AppBase app, string uniqueId, string message)
		{
			if( !MarkAsUnique(uniqueId, message) )
				return;

			DebugMessage(app, message);
		}

		internal static void DebugUniqueMessage(string uniqueId, string message, string name)
		{
			if( !MarkAsUnique(uniqueId, message) )
				return;

			DebugMessage(message, name);
		}

		/// <summary>
		/// Print a debug message on the console and save it to the log file.
		/// </summary>
		/// <param name="app">Current application instance.</param>
		/// <param name="msg">String containing the message to be saved and displayed.</param>
		public static void DebugMessage(AppBase app, string msg)
		{
			if( app == null || app.GetAppInfo() == null )
				return;

			AppInfo appInfo = app.GetAppInfo();
			string AppName	= appInfo.AppName;
			string AppID	= appInfo.AppID;
			string message	= string.Format("[{0} - {1}] {2}", AppName, DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), msg);

			Game.Console.Print(message);

			WriteLogFile(AppID, message);
		}

		internal static void DebugMessage(string msg, string name = "")
		{
			if( string.IsNullOrWhiteSpace(msg) )
				return;

			if( string.IsNullOrWhiteSpace(name) )
				name = LCPDDevice.APP_NAME;

			string message = string.Format("[{0} - {1}] {2}", name, DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), msg);

			Game.Console.Print(message);

			WriteLogFile(name, message);
		}

		private static bool MarkAsUnique(string uniqueId, string message)
		{
			if( debugWatcher[uniqueId] != null && debugWatcher[uniqueId] == message )
				return false;

			debugWatcher[uniqueId] = message;
			return true;
		}
	}
}
