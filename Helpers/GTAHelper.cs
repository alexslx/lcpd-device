﻿using System;
using GTA;
using LCPDDevice.API.Collections;
using LCPDDevice.API.Managers;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// General functions related to GTA
	/// </summary>
	public class GTAHelper
	{
		private const string DEFAULT_STRING = "Unknown";

		/// <summary>
		/// Get zone name in human readable form.
		/// </summary>
		/// <param name="position">Vector3 containing the desired zone position.</param>
		/// <param name="Default">Default value if the zone is not found.</param>
		/// <returns>String containing the zone name or Default when not found.</returns>
		public static string GetZoneNameByPosition(Vector3 position, string Default = DEFAULT_STRING)
		{
			return GetZoneNameById(World.GetZoneName(position), Default);
		}

		/// <summary>
		/// Get zone name in human readable form.
		/// </summary>
		/// <param name="id">String containing the zone id.</param>
		/// <param name="Default">Default value if the zone is not found.</param>
		/// <returns>String containing the zone name or Default when not found.</returns>
		public static string GetZoneNameById(string id, string Default = DEFAULT_STRING)
		{
			try
			{
				GTACollection zones		= GTACollectionManager.GetInstance().GetCollection("ZONES");
				GTACollectionItem item	= zones.GetItemById(id);
				if( item != null )
					return item.GetValue(Default);
			}
			catch( Exception )
			{
			}
			return Default;
		}
	}
}
