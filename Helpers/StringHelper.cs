﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using GTA;
using System.Text.RegularExpressions;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// General converter for strings.
	/// </summary>
	public class StringHelper
	{
		/// <summary>
		/// Convert a string to a boolean Value. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static bool ReadSafeBool(string Value, bool Default)
		{
			try
			{
				bool result = Convert.ToBoolean(Value);
				return result;
			}
			catch( Exception )
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to an integer Value. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static int ReadSafeInt(string Value, int Default)
		{
			int result = Default;
			if( Int32.TryParse(Value, out result) )
				return result;
			else
				return Default;
		}

		/// <summary>
		/// Convert a string to a floating Value. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static float ReadSafeFloat(string Value, float Default)
		{
			try
			{
				Value = Value.Replace("f", string.Empty).Replace("F", string.Empty).Trim();

				float result = float.Parse(Value, CultureInfo.InvariantCulture.NumberFormat);
				return result;
			}
			catch( Exception )
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a double Value. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static double ReadSafeDouble(string Value, double Default)
		{
			try
			{
				Value = Value.Replace("D", string.Empty).Replace("d", string.Empty).Trim();

				double result = double.Parse(Value, CultureInfo.InvariantCulture.NumberFormat);
				return result;
			}
			catch( Exception )
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a <see cref="Color"/> structure. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static Color ReadSafeColor(string Value, Color Default)
		{
			try
			{
				Color result = ColorTranslator.FromHtml(Value);
				return result;
			}
			catch(Exception)
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a <see cref="Point"/> structure. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static Point ReadSafePoint(string Value, Point Default)
		{
			try
			{
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(Point));
				Point result = (Point)converter.ConvertFromString(Value);
				return result;
			}
			catch(Exception)
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a <see cref="Size"/> structure. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static Size ReadSafeSize(string Value, Size Default)
		{
			try
			{
				int w = 0, h = 0;
				string[] p = Value.Split(',');
				bool test  = Int32.TryParse(p[0], out w) && Int32.TryParse(p[1], out h);
				if( test )
					return new Size(w, h);
				else
					return Default;
			}
			catch(Exception)
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a Vector3 structure. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static Vector3 ReadSafeVector(string Value, Vector3 Default)
		{
			try
			{
				float x = 0f, y = 0f, z = 0f;

				Value = Value.Replace("(", string.Empty).Trim();
				Value = Value.Replace(")", string.Empty).Trim();

				string[] p = Value.Split(',');
				x = ReadSafeFloat(p[0].Trim(), 0f);
				y = ReadSafeFloat(p[1].Trim(), 0f);
				z = ReadSafeFloat(p[2].Trim(), 0f);

				Vector3 result = new Vector3(x, y, z);
				return result;
			}
			catch( Exception )
			{
				return Default;
			}
		}

		/// <summary>
		/// Convert a string to a <see cref="Keys"/> <see cref="Enum"/> appType. Uses Default parameter in case of failure.
		/// </summary>
		/// <param name="Value">String to be converted.</param>
		/// <param name="Default">Default Value to be used if the conversion fails.</param>
		/// <returns>Converted Value or default Value if any failure occurs.</returns>
		public static Keys ReadSafeKey(string Value, Keys Default)
		{
			try
			{
				Keys result;
				if( Enum.TryParse(Value, out result) )
					return result;

				return Default;
			}
			catch(Exception)
			{
				return Default;
			}
		}

		/// <summary>
		/// Check if the given string is a valid id (a-zA-Z0-9_) string.
		/// </summary>
		/// <param name="Value">String to be valided</param>
		/// <returns>True or false</returns>
		public static bool IsValidID(string Value)
		{
			if( string.IsNullOrEmpty(Value) )
				return false;

			Regex r = new Regex("^[a-zA-Z0-9_]*$");

			return r.IsMatch(Value);
		}

		/// <summary>
		/// Parse a string to a version array.
		/// String must be in the following format: X.Y.Z.W
		/// </summary>
		/// <param name="versionArray">Reference to an array to store the parsed values.</param>
		/// <param name="versionStr">Desired string to be parsed.</param>
		public static void ParseVersion(ref int[] versionArray, string versionStr)
		{
			string[] str = versionStr.Split('.');

			for( int i = 0; i < Math.Min(4, str.Length); i++ )
			{
				int n = StringHelper.ReadSafeInt(str[i], versionArray[i]);
				versionArray[i] = n;
			}
		}
	}
}
