﻿using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace LCPDDevice.Helpers
{
	/// <summary>
	/// Helper class to read an Ini file.
	/// </summary>
	public class IniFile
	{
		private string path;

		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

		/// <summary>
		/// INIFile Constructor.
		/// </summary>
		/// <param name="INIPath">String containing the path of the ini file.</param>
		public IniFile(string INIPath)
		{
			path = INIPath;
		}

		/// <summary>
		/// Write Data to the INI File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		private void WriteValue(string Section, string Key, string Value)
		{
			WritePrivateProfileString(Section, Key, Value, this.path);
		}

		/// <summary>
		/// Read Data Value From the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>String containing the read value or default value in case of any errors.</returns>
		private string ReadValue(string Section, string Key, string Default)
		{
			StringBuilder temp = new StringBuilder(255);
			int i = GetPrivateProfileString(Section, Key, Default, temp, 255, this.path);
			return temp.ToString();

		}

		/// <summary>
		/// Write a string value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteString(string Section, string Key, string Value)
		{
			WritePrivateProfileString(Section, Key, Value, this.path);
		}

		/// <summary>
		/// Read a string value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public string ReadString(string Section, string Key, string Default)
		{
			return ReadValue(Section, Key, Default);
		}

		/// <summary>
		/// Write an integer value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteInt(string Section, string Key, int Value)
		{
			WriteString(Section, Key, Value.ToString());
		}

		/// <summary>
		/// Read an integer value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public int ReadInteger(string Section, string Key, int Default)
		{
			string tmp = ReadValue(Section, Key, Default.ToString());

			return StringHelper.ReadSafeInt(tmp, Default);
		}

		/// <summary>
		/// Write a float value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteFloat(string Section, string Key, float Value)
		{
			WriteString(Section, Key, Value.ToString());
		}

		/// <summary>
		/// Read a floating value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public float ReadFloat(string Section, string Key, float Default)
		{
			string tmp = ReadValue(Section, Key, Default.ToString());

			return StringHelper.ReadSafeFloat(tmp, Default);
		}

		/// <summary>
		/// Write a double value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteDouble(string Section, string Key, double Value)
		{
			WriteString(Section, Key, Value.ToString());
		}

		/// <summary>
		/// Read a double value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public double ReadDouble(string Section, string Key, double Default)
		{
			string tmp = ReadValue(Section, Key, Default.ToString());

			return StringHelper.ReadSafeDouble(tmp, Default);
		}

		/// <summary>
		/// Write a boolean value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteBoolean(string Section, string Key, bool Value)
		{
			WriteString(Section, Key, Value.ToString());
		}

		/// <summary>
		/// Read a boolean value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public bool ReadBoolean(string Section, string Key, bool Default)
		{
			string tmp = ReadValue(Section, Key, Default.ToString());

			return StringHelper.ReadSafeBool(tmp, Default);
		}

		/// <summary>
		/// Write a <see cref="Keys"/> value to the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be written.</param>
		/// <param name="Key">String containing the name of the key to be written.</param>
		/// <param name="Value">String containing the value to be written.</param>
		public void WriteKey(string Section, string Key, Keys Value)
		{
			WriteString(Section, Key, Value.ToString());
		}

		/// <summary>
		/// Read a <see cref="Keys"/> value from the Ini File
		/// </summary>
		/// <param name="Section">String containing the name of the section to be read.</param>
		/// <param name="Key">String containing the name of the key to be read.</param>
		/// <param name="Default">String containing the default value to be set in case of any errors.</param>
		/// <returns>Value loaded from the Ini File or Default if any error occurs</returns>
		public Keys ReadKey(string Section, string Key, Keys Default)
		{
			string tmp = ReadValue(Section, Key, Default.ToString());

			return StringHelper.ReadSafeKey(tmp, Default);
		}
	}
}
