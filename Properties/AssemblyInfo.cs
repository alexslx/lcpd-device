﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LCPDDevice")]
[assembly: AssemblyDescription("GTA IV Plug-in developed to use with ScriptHookDotNet")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("LCPDDevice")]
[assembly: AssemblyCopyright("Copyright © 2014 Alexandre Leites")]
[assembly: AssemblyTrademark("Alexandre Leites <http://www.alexslx.com.br/>")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the appTypeList in this assembly not visible 
// to COM components.  If you need to access a appType in this assembly from 
// COM, set the ComVisible attribute to true on that appType.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8d78c42c-5828-4435-8905-0c90623deabe")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.3.0")]
[assembly: AssemblyFileVersion("0.0.3.0")]
